package com.hs.VO;

import lombok.Data;

import java.util.Date;
@Data
public class OfferFlushVO {
    private Integer id;

    private Integer goodId;

    private Integer limitTime;

    private Byte status;

    private Double initPrice;

    private String goodName;

    private Integer userId;

    private Integer offerId;

    private Double price;

    private Date createTime;

    private Double highestPrice;

    private Date updateTime;

}
