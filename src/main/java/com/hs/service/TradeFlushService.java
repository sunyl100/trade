package com.hs.service;

import com.hs.config.PageObj;
import com.hs.dao.TradeFlushMapper;
import com.hs.entity.Trade;
import com.hs.entity.TradeFlush;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TradeFlushService {
    @Autowired
    TradeFlushMapper tradeFlushMapper;
    public PageObj page(TradeFlush obj){
        int total = tradeFlushMapper.selectTradeFlushByConditionCount(obj);
        if(total == 0){
            return new PageObj(obj.getCurrentPage(), obj.getPageSize(),total,null);
        }
        List<TradeFlush> list = tradeFlushMapper.selectTradeFlushByCondition(obj);
        return new PageObj(obj.getCurrentPage(), obj.getPageSize(),total,list);
    }
    public List<TradeFlush> listLimit(TradeFlush obj){
        if(obj.getLimit() == null){
            obj.setLimit(10);
        }
        List<TradeFlush> list = tradeFlushMapper.selectTradeFlushByCondition(obj);
        return list;
    }
}
