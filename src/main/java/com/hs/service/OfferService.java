package com.hs.service;

import com.hs.VO.OfferFlushVO;
import com.hs.config.PageObj;
import com.hs.dao.OfferMapper;
import com.hs.entity.Offer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OfferService {
    @Autowired
    OfferMapper offerMapper;
    public int insert(Offer offer){
        offer.setCreateTime(new Date());
        return offerMapper.insertSelective(offer);
    }
    public List<Offer> list(Offer offer){
        return offerMapper.selectByCondition(offer);
    }
    public PageObj page(Offer offer){
        int total = offerMapper.selectByConditionCount(offer);
        if(total == 0){
            return new PageObj(offer.getCurrentPage(), offer.getPageSize(),total,null);
        }
        List<Offer> list = offerMapper.selectByCondition(offer);
        return new PageObj(offer.getCurrentPage(), offer.getPageSize(),total,list);
    }
    public PageObj pageOfferWithFLush(Offer offer){
        int total = offerMapper.selectOfferWithFlushCount(offer);
        if(total == 0){
            return new PageObj(offer.getCurrentPage(), offer.getPageSize(),total,null);
        }
        List<OfferFlushVO> list = offerMapper.selectOfferWithFlush(offer);
        return new PageObj(offer.getCurrentPage(), offer.getPageSize(),total,list);
    }

}
