package com.hs.service;

import com.hs.config.PageObj;
import com.hs.dao.FundFlushMapper;
import com.hs.entity.FundFlush;
import com.hs.entity.TradeFlush;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FundFlushService {
    @Autowired
    FundFlushMapper fundFlushMapper;
    public PageObj page(FundFlush obj){
        int total = fundFlushMapper.selectFundFlushByConditionCount(obj);
        if(total == 0){
            return new PageObj(obj.getCurrentPage(), obj.getPageSize(),total,null);
        }
        List<FundFlush> list = fundFlushMapper.selectFundFlushByCondition(obj);
        return new PageObj(obj.getCurrentPage(), obj.getPageSize(),total,list);
    }
}
