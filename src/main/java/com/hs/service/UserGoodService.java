package com.hs.service;

import com.hs.VO.OfferFlushVO;
import com.hs.config.PageObj;
import com.hs.dao.UserGoodMapper;
import com.hs.entity.Offer;
import com.hs.entity.UserGood;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserGoodService {
    @Autowired
    UserGoodMapper userGoodMapper;
    public int insert(UserGood userGood){
        return userGoodMapper.insert(userGood);
    }
    public int update(UserGood userGood){
        return userGoodMapper.update(userGood);
    }
    public PageObj pageUserGood(UserGood offer){
        int total = userGoodMapper.selectUserGoodCount(offer);
        if(total == 0){
            return new PageObj(offer.getCurrentPage(), offer.getPageSize(),total,null);
        }
        List<UserGood> list = userGoodMapper.selectUserGoodList(offer);
        return new PageObj(offer.getCurrentPage(), offer.getPageSize(),total,list);
    }



}
