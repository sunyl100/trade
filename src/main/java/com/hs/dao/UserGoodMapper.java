package com.hs.dao;

import com.hs.entity.UserGood;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserGoodMapper {
    int insert(UserGood record);

    int insertSelective(UserGood record);

    int selectUserGoodCount(UserGood userGood);

    int update(UserGood userGood);

    List<UserGood> selectUserGoodList(UserGood userGood);

    UserGood getUserGood(UserGood userGood);

}