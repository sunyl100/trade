package com.hs.dao;

import com.hs.entity.TradePlate;
import org.springframework.stereotype.Repository;

@Repository
public interface TradePlateMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TradePlate record);

    int insertSelective(TradePlate record);

    TradePlate selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TradePlate record);

    int updateByPrimaryKey(TradePlate record);
}