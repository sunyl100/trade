package com.hs.dao;

import com.hs.entity.OfferFlush;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OfferFlushMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(OfferFlush record);

    int insertSelective(OfferFlush record);

    OfferFlush selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(OfferFlush record);

    int editFlush(OfferFlush record);

    int updateByPrimaryKey(OfferFlush record);

    List<OfferFlush> selectByCondition(OfferFlush record);


}