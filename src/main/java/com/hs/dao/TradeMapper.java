package com.hs.dao;

import com.hs.VO.OfferFlushVO;
import com.hs.entity.Offer;
import com.hs.entity.Trade;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TradeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Trade record);

    int insertSelective(Trade record);

    Trade selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Trade record);

    int updateByPrimaryKey(Trade record);

    List<Trade> selectTradeByCondition(Trade offer);
    int selectTradeByConditionCount(Trade offer);

    List<Trade> selectTradeByConditionForTrade(Trade offer);

    int deleteReptile();
}