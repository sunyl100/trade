package com.hs.dao;

import com.hs.entity.Trade;
import com.hs.entity.TradeFlush;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface TradeFlushMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TradeFlush record);

    int insertSelective(TradeFlush record);

    TradeFlush selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TradeFlush record);

    int updateByPrimaryKey(TradeFlush record);


    List<TradeFlush> selectTradeFlushByCondition(TradeFlush offer);

    int selectTradeFlushByConditionCount(TradeFlush offer);

    TradeFlush getTradeFlushLast();


}