package com.hs.dao;

import com.hs.entity.FundFlush;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FundFlushMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(FundFlush record);

    int insertSelective(FundFlush record);

    FundFlush selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(FundFlush record);

    int updateByPrimaryKey(FundFlush record);

    int selectFundFlushByConditionCount(FundFlush record);

    List<FundFlush> selectFundFlushByCondition(FundFlush record);
}