package com.hs.dao;

import com.hs.VO.OfferFlushVO;
import com.hs.entity.Offer;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface OfferMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Offer record);

    int insertSelective(Offer record);

    Offer selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Offer record);

    int updateByPrimaryKey(Offer record);

    List<Offer> selectByCondition(Offer record);

    int selectByConditionCount(Offer record);


    List<OfferFlushVO> selectOfferWithFlush(Offer offer);

    int selectOfferWithFlushCount(Offer offer);
}