package com.hs.controller;

import com.hs.config.DateUtil;
import com.hs.config.ExcelUtil;
import com.hs.config.R;
import com.hs.dao.TradeFlushMapper;
import com.hs.entity.OfferFlush;
import com.hs.entity.TradeFlush;
import com.hs.service.TradeFlushService;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

@Controller
@RequestMapping("tradeFlush")
public class TradeFlushController {

    @Autowired
    TradeFlushMapper tradeFlushMapper;
    @Autowired
    TradeFlushService tradeFlushService;


    @RequestMapping("tradeFlushLineData")
    @ResponseBody
    public R tradeFlushLineData(Integer limit){
        TradeFlush tradeFlush = new TradeFlush();
        tradeFlush.setLimit(limit);
        List<TradeFlush> tradeFlushList = tradeFlushService.listLimit(tradeFlush);
        if(tradeFlushList!=null && tradeFlushList.size() > 0){
            return R.SUCCESS("",tradeFlushList);
        }
        return R.FAILURE("无数据",null);
    }
    @RequestMapping("excelOut")
    @ResponseBody
    public void excelOut(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
        TradeFlush offerFlush = new TradeFlush();
        List<TradeFlush> list = tradeFlushMapper.selectTradeFlushByCondition(offerFlush);
        //excel文件名
        String fileName = "成交流水"+System.currentTimeMillis()+".xls";
        //sheet名
        String sheetName = "成交流水";
        String [] title = {"序列","物品","价格","数量","买方","卖方","成交时间"};
        String [][] content =  new String[list.size()][title.length];
        for (int i = 0; i < list.size(); i++) {
            content[i] = new String[title.length];
            TradeFlush obj = list.get(i);
            content[i][0] = String.valueOf(i);
            content[i][1] = obj.getGoodName();
            content[i][2] = obj.getPrice().toString();
            content[i][3] = obj.getAmount().toString();
            content[i][4] = obj.getBuyerName();
            content[i][5] = obj.getSellerName();
            content[i][6] = DateUtil.FormatDate(obj.getCreateTime());
        }
        //创建HSSFWorkbook
        HSSFWorkbook wb = ExcelUtil.getHSSFWorkbook(sheetName, title, content, null);
        //响应到客户端
        try {
            this.setResponseHeader(response, fileName);
            OutputStream os = response.getOutputStream();
            wb.write(os);
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //发送响应流方法
    public void setResponseHeader(HttpServletResponse response, String fileName) {
        try {
            try {
                fileName = new String(fileName.getBytes(),"ISO8859-1");
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            response.setContentType("application/octet-stream;charset=ISO8859-1");
            response.setHeader("Content-Disposition", "attachment;filename="+ fileName);
            response.addHeader("Pargam", "no-cache");
            response.addHeader("Cache-Control", "no-cache");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


}
