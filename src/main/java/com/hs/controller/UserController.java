package com.hs.controller;

import com.hs.config.R;
import com.hs.entity.Offer;
import com.hs.entity.User;
import com.hs.service.UserService;
import org.apache.poi.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("user")
public class UserController {
    @Autowired
    UserService userService;
    @RequestMapping("selectAll")
    @ResponseBody
    public List<User> selectAll(User map){
        return userService.selectByCondition(map);
    }
    @RequestMapping("insert")
    @ResponseBody
    public R insert(User user, Model  model) {
        if(user == null){
//            model.addAttribute("msg","请填写用户名密码等关键信息");
            return R.FAILURE("请填写用户名密码等关键信息",null);
        }
//        if(user.getFund() == null){
////            model.addAttribute("msg","请填写用户名密码等关键信息");
//            return R.FAILURE("请填写资产金额",null);
//        }
        if(user.getName() == null || user.getPassWord().trim() == ""){
            model.addAttribute("msg","请填写用户名");
            return R.FAILURE("请填写用户名",null);
        }
        if(user.getPassWord() == null || user.getPassWord().trim() == ""){
            model.addAttribute("msg","请填写密码");
            return R.FAILURE("请填写密码",null);
        }
        List<User> list = userService.selectByCondition(user);
        if(list.size() != 0){
            return R.FAILURE("用户已存在",null);
        }
        int i = userService.insertSelective(user);
        if(i > 0){
            return R.SUCCESS("",i);
        }
        return R.FAILURE("",0);
    }
    @RequestMapping("edit")
    @ResponseBody
    public R edit(@RequestParam Integer id, Double fund) {
        if(id == null){
//            model.addAttribute("msg","请填写用户名密码等关键信息");
            return R.FAILURE("用户id不能为空",null);
        }
        if(fund == null){
//            model.addAttribute("msg","请填写用户名密码等关键信息");
            return R.FAILURE("请填写资产金额",null);
        }
        User user = new User();
        user.setId(id);
        user.setFund(fund);
        user.setUpdateTime(new Date());
        int i = userService.updateByPrimaryKeySelective(user);
        if(i > 0){
            return R.SUCCESS("",i);
        }
        return R.FAILURE("用户更新失败",0);
    }

    @ResponseBody
    @RequestMapping("page")
    public R page(User offer){
        if(offer.getPageSize() == 0){
            offer.setPageSize(15);
        }
        return R.SUCCESS("",userService.page(offer));
    }

}
