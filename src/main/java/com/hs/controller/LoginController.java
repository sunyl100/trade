package com.hs.controller;

import com.hs.config.R;
import com.hs.entity.User;
import com.hs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;
@Controller
@RequestMapping("login")
public class LoginController {
    @Autowired
    UserService userService;
    @RequestMapping("action")
    @ResponseBody
    public R l(User user,HttpSession session){
        user.setPageStart(null);
        List<User> list = userService.selectByCondition(user);
        if(list.size() == 0){
            return R.FAILURE("用户不存在",0);
        }
        User u = list.get(0);
        session.setAttribute("user",u);
        return R.SUCCESS("成功",u);
    }





}
