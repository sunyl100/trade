package com.hs.controller;

import com.hs.config.DateUtil;
import com.hs.config.ExcelUtil;
import com.hs.config.R;
import com.hs.entity.OfferFlush;
import com.hs.entity.OfferFlushService;
import com.hs.entity.User;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;


@Controller
@RequestMapping("offerFlush")
public class OfferFLushController {
    @Autowired
    OfferFlushService offerFlushService;
    @RequestMapping("insert")
    @ResponseBody
    public R insert(@RequestParam Integer offerId, Double price, HttpSession session){
        User user = (User) session.getAttribute("user");
        if(user == null){
            return R.FAILURE("请登录",0);
        }
        if(offerId== null){
            return R.FAILURE("getOfferId不能为空",0);
        }
        if(price == null){
            return R.FAILURE("price不能为空",0);
        }
        OfferFlush offerFlush = new OfferFlush();
        offerFlush.setOfferId(offerId);
        offerFlush.setPrice(price);
        offerFlush.setUserId(user.getId());
        int i = offerFlushService.insert(offerFlush);
        if(i > 0){
            return R.SUCCESS("",i);
        }
        return R.FAILURE("",0);
    }
    @RequestMapping("edit")
    @ResponseBody
    public R edit(@RequestParam Integer offerId, Double price, HttpSession session){
        User user = (User) session.getAttribute("user");
        if(user == null){
            return R.FAILURE("请登录",0);
        }
        if(offerId== null){
            return R.FAILURE("getOfferId不能为空",0);
        }
        if(price == null){
            return R.FAILURE("price不能为空",0);
        }

        OfferFlush offerFlush = new OfferFlush();
        offerFlush.setOfferId(offerId);
        offerFlush.setPrice(price);
        offerFlush.setUserId(user.getId());

        int i = offerFlushService.editFlush(offerFlush);
        if(i > 0){
            return R.SUCCESS("",i);
        }
        return R.FAILURE("",0);
    }

    @RequestMapping("excelOut")
    @ResponseBody
    public void excelOut(HttpServletRequest request,HttpServletResponse response) throws UnsupportedEncodingException {
        OfferFlush offerFlush = new OfferFlush();
        String offerId = request.getParameter("offerId");
        offerFlush.setOfferId(Integer.valueOf(offerId));
        List<OfferFlush> list = offerFlushService.list(offerFlush);
        //excel文件名
        String fileName = "报价"+System.currentTimeMillis()+".xls";
        //sheet名
        String sheetName = "报价信息";
        String [] title = {"序列","拍卖者","报价","报价的时间"};
        String [][] content =  new String[list.size()][title.length];
        for (int i = 0; i < list.size(); i++) {
            content[i] = new String[title.length];
            OfferFlush obj = list.get(i);
            content[i][0] = String.valueOf(i);
            content[i][1] = obj.getUserName();
            content[i][2] = obj.getPrice().toString();
            content[i][3] = DateUtil.FormatDate(obj.getCreateTime());
        }
        //创建HSSFWorkbook
        HSSFWorkbook wb = ExcelUtil.getHSSFWorkbook(sheetName, title, content, null);
        //响应到客户端
        try {
            this.setResponseHeader(response, fileName);
               OutputStream os = response.getOutputStream();
               wb.write(os);
               os.flush();
               os.close();
         } catch (Exception e) {
               e.printStackTrace();
         }

    }

    //发送响应流方法
    public void setResponseHeader(HttpServletResponse response, String fileName) {
        try {
            try {
                fileName = new String(fileName.getBytes(),"ISO8859-1");
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            response.setContentType("application/octet-stream;charset=ISO8859-1");
            response.setHeader("Content-Disposition", "attachment;filename="+ fileName);
            response.addHeader("Pargam", "no-cache");
            response.addHeader("Cache-Control", "no-cache");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


}
