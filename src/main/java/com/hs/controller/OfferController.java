package com.hs.controller;

import com.hs.config.R;
import com.hs.entity.Offer;
import com.hs.service.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("offer")
@RestController
public class OfferController {
    @Autowired
    OfferService offerService;
    @RequestMapping("insert")
    public R insert(Offer offer){
        if(offer.getGoodId() == null){
            return R.FAILURE("请选择物品",0);
        }
        if(offer.getLimitTime() == null){
            return R.FAILURE("请选择限时",0);
        }
        if(offer.getInitPrice() == null){
            return R.FAILURE("请选择基础价格",0);
        }

        int i = offerService.insert(offer);
        if(i > 0){
            return R.SUCCESS("",i);
        }
        return R.FAILURE("",0);
    }

    @RequestMapping("page")
    public R page(Offer offer){
        if(offer.getPageSize() == 0){
            offer.setPageSize(15);
        }
        return R.SUCCESS("",offerService.page(offer));
    }

}
