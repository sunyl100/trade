package com.hs.controller;

import com.hs.config.MyValues;
import com.hs.config.R;
import com.hs.dao.GoodMapper;
import com.hs.dao.OfferMapper;
import com.hs.entity.*;
import com.hs.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * 路由
 */
@Controller
public class IndexController {
    @Autowired
    GoodMapper goodMapper;
    @Autowired
    OfferService offerService;
    @Autowired
    TradeService tradeService;
    @Autowired
    TradeFlushService tradeFlushService;
    @Autowired
    FundFlushService fundFlushService;
    @Autowired
    UserGoodService userGoodService;
    @Autowired
    OfferFlushService offerFlushService;
    @Autowired
    UserService userService;
    @Autowired
    OfferMapper offerMapper;

    @RequestMapping("/")
    public String index(HttpSession session,Model model){
        session.setAttribute("user",session.getAttribute("user"));
        model.addAttribute("user",session.getAttribute("user"));
        model.addAttribute("taskValue",new MyValues());

        return "index";
    }
    @RequestMapping("/task")
    public String task(HttpSession session,Model model){
        model.addAttribute("taskValue", new MyValues());
        return "task";
    }
    @ResponseBody
    @RequestMapping("/tradePlatePriceEdit")
    public R tradePlatePrice(HttpServletRequest request, Model model){
        String price = request.getParameter("price");
        MyValues.TRADE_PLATE_PRICE = Double.valueOf(price);
        return R.SUCCESS("",1);
    }
    @RequestMapping("/task/taskOffer")
    public String taskOffer(HttpServletRequest request, Model model){
        Integer flag = Integer.valueOf(request.getParameter("flag")) ;
        if(flag == 1){
            MyValues.task_offer = true;
        }else{
            MyValues.task_offer = false;
        }
        model.addAttribute("taskValue", new MyValues());
        return "task";
    }
    @RequestMapping("/task/taskTrade")
    public String taskTrade(HttpServletRequest request,Model model){
        Integer flag =Integer.valueOf(request.getParameter("flag")) ;
        if(flag == 1){
            MyValues.task_trade = true;
        }else{
            MyValues.task_trade = false;
        }
        model.addAttribute("taskValue", new MyValues());
        return "task";
    }
    @RequestMapping("/task/taskTradePlate")
    public String taskTradePlate(HttpServletRequest request,Model model){
        Integer flag =Integer.valueOf(request.getParameter("flag")) ;
        if(flag == 1){
            MyValues.Task_PLATE_PRICE = true;
        }else{
            MyValues.Task_PLATE_PRICE = false;
        }
        model.addAttribute("taskValue", new MyValues());
        return "task";
    }
    /* 爬虫任务*/
    @RequestMapping("/task/reptile")
    public String reptile(HttpServletRequest request,Model model){
        Integer flag =Integer.valueOf(request.getParameter("flag")) ;
        if(flag == 1){
            MyValues.reptile = true;
        }else{
            MyValues.reptile = false;
        }
        if(MyValues.reptile == false){
            tradeService.deleteReptile();
        }
        model.addAttribute("taskValue", new MyValues());
        return "task";
    }


    @RequestMapping("login")
    public String login(){
        return "login";
    }
    @RequestMapping("loginOut")
    public String loginOut(HttpSession session){
        session.invalidate();
        return "login";
    }
    //用户
    @RequestMapping("user/info")
    public String info(HttpSession session,Model model){
        User user = (User) session.getAttribute("user");
        model.addAttribute("user",user);
        //持有资产
        //交易记录
        return "user/info";
    }
    @RequestMapping("user/add")
    public String userAdd(){
        return "user/userAdd";
    }
    @RequestMapping("user/list")
    public String userList(User user, Model model){
        if(user.getPageSize() == null){
            user.setPageSize(15);
        }
        if(user.getCurrentPage() == null ){
            user.setCurrentPage(0);
        }
        if(user.getFlag() == null){
            user.setFlag(0);
        }
        user.setCurrentPage(user.getCurrentPage() + user.getFlag());
        user.setPageStart(user.getCurrentPage() *  user.getPageSize());
        model.addAttribute("page",userService.page(user));
        return "user/userList";
    }
    //报价 offer
    @RequestMapping("offer/add")
    public String offerAdd(Model model){
        model.addAttribute("goods",goodMapper.selectByCondition(null));
        return "offer/offerAdd";
    }
    //出价列表
    @RequestMapping("offer/offerWithFlush")
    public String offerWithFlush(Offer user, Model model,HttpSession session){
        User user1 = (User) session.getAttribute("user");
        if(user.getPageSize() == null){
            user.setPageSize(15);
        }
        if(user.getCurrentPage() == null ){
            user.setCurrentPage(0);
        }
        if(user.getFlag() == null){
            user.setFlag(0);
        }
        user.setUserId(user1.getId());
        user.setStatus(new Integer(1).byteValue());
        user.setCurrentPage(user.getCurrentPage() + user.getFlag());
        user.setPageStart(user.getCurrentPage() *  user.getPageSize());
        model.addAttribute("page",offerService.pageOfferWithFLush(user));
        return "offer/offerWithFlush";
    }
    //公示列表
    @RequestMapping("offer/list")
    public String offerList(Offer user, Model model,HttpSession session){
        User user1 = (User) session.getAttribute("user");
        if(user.getPageSize() == null){
            user.setPageSize(15);
        }
        if(user.getCurrentPage() == null ){
            user.setCurrentPage(0);
        }
        if(user.getFlag() == null){
            user.setFlag(0);
        }
        user.setUserId(user1.getId());
        user.setStatus(new Integer(2).byteValue());
        user.setCurrentPage(user.getCurrentPage() + user.getFlag());
        user.setPageStart(user.getCurrentPage() *  user.getPageSize());
        model.addAttribute("page",offerService.page(user));
        return "offer/offerList";
    }
    @RequestMapping("offer/offerFlushDetail")
    public String offerFlushDetail(OfferFlush user, Model model, HttpSession session){
        if(user.getOfferId() == null){
            model.addAttribute("mes","offerId不能为空");
            return "error";
        }
        model.addAttribute("offerId",user.getOfferId());
        model.addAttribute("list",offerFlushService.list(user));
        return "offer/offerFlushDetail";
    }

    //交易 trade
    @RequestMapping("trade/buyAdd")
    public String buyDdd(Model model){
        model.addAttribute("goods",goodMapper.selectByCondition(null));
        return "trade/tradeBuyAdd";
    }
    @RequestMapping("trade/sellAdd")
    public String sellAdd(Model model){
        model.addAttribute("goods",goodMapper.selectByCondition(null));
        return "trade/tradeSellAdd";
    }
    @RequestMapping("/trade/buyList")
    public String tradeBuyList(Model model,HttpSession session,Trade obj){
        User user1 = (User) session.getAttribute("user");
        if(obj.getPageSize() == null){
            obj.setPageSize(15);
        }
        if(obj.getCurrentPage() == null ){
            obj.setCurrentPage(0);
        }
        if(obj.getFlag() == null){
            obj.setFlag(0);
        }
        obj.setType(new Integer(1).byteValue());//1买 2卖
        obj.setUserId(user1.getId());
        obj.setCurrentPage(obj.getCurrentPage() + obj.getFlag());//控制翻页
        obj.setPageStart(obj.getCurrentPage() *  obj.getPageSize());
        model.addAttribute("page",tradeService.page(obj));
        model.addAttribute("goods",goodMapper.selectByCondition(null));
        return "trade/tradeBuyList";
    }
    @RequestMapping("/tradeFlush/list")
    public String tradeFlushList(Model model, HttpSession session, TradeFlush obj){
//        User user1 = (User) session.getAttribute("user");
        if(obj.getPageSize() == null){
            obj.setPageSize(15);
        }
        if(obj.getCurrentPage() == null ){
            obj.setCurrentPage(0);
        }
        if(obj.getFlag() == null){
            obj.setFlag(0);
        }
//        obj.setUserId(user1.getId());
        obj.setCurrentPage(obj.getCurrentPage() + obj.getFlag());//控制翻页
        obj.setPageStart(obj.getCurrentPage() *  obj.getPageSize());
        model.addAttribute("page",tradeFlushService.page(obj));
        return "tradeFlush/tradeFlushList";
    }
    @RequestMapping("/tradeFlush/line")
    public String tradeFlushLine(Model model, HttpSession session, TradeFlush obj){
//        User user1 = (User) session.getAttribute("user");
        if(obj.getPageSize() == null){
            obj.setPageSize(15);
        }
        if(obj.getCurrentPage() == null ){
            obj.setCurrentPage(0);
        }
        if(obj.getFlag() == null){
            obj.setFlag(0);
        }
//        obj.setUserId(user1.getId());
        model.addAttribute("page",tradeFlushService.page(obj));
        return "tradeFlush/tradeLine";
    }
    @RequestMapping("/trade/sellList")
    public String tradeSellList(Model model,HttpSession session,Trade obj){
        User user1 = (User) session.getAttribute("user");
        if(obj.getPageSize() == null){
            obj.setPageSize(15);
        }
        if(obj.getCurrentPage() == null ){
            obj.setCurrentPage(0);
        }
        if(obj.getFlag() == null){
            obj.setFlag(0);
        }
        obj.setType(new Integer(2).byteValue());
        obj.setUserId(user1.getId());
        obj.setCurrentPage(obj.getCurrentPage() + obj.getFlag());//控制翻页
        obj.setPageStart(obj.getCurrentPage() *  obj.getPageSize());
        model.addAttribute("page",tradeService.page(obj));
        return "trade/tradeSellList";
    }

    @RequestMapping("/trade/list")
    public String tradeList(Model model,HttpSession session,Trade obj){
        if(obj.getPageSize() == null){
            obj.setPageSize(15);
        }
        if(obj.getCurrentPage() == null ){
            obj.setCurrentPage(0);
        }
        if(obj.getFlag() == null){
            obj.setFlag(0);
        }
        obj.setStatus(0);
        obj.setCurrentPage(obj.getCurrentPage() + obj.getFlag());//控制翻页
        obj.setPageStart(obj.getCurrentPage() *  obj.getPageSize());
        model.addAttribute("page",tradeService.page(obj));
        return "trade/tradeList";
    }
    @RequestMapping("userGood/add")
    public String userGoodAdd(Model model){
        model.addAttribute("goods",goodMapper.selectByCondition(null));
        return "userGood/userGoodAdd";
    }
    @RequestMapping("userGood/list")
    public String userGoodList(UserGood user,Model model, HttpSession session){
        User user1 = (User) session.getAttribute("user");
        if(user.getPageSize() == null){
            user.setPageSize(15);
        }
        if(user.getCurrentPage() == null ){
            user.setCurrentPage(0);
        }
        if(user.getFlag() == null){
            user.setFlag(0);
        }
        user.setUserId(user1.getId());
        user.setCurrentPage(user.getCurrentPage() + user.getFlag());//控制翻页
        user.setPageStart(user.getCurrentPage() *  user.getPageSize());
        model.addAttribute("page",userGoodService.pageUserGood(user));
        return "userGood/userGoodList";
    }

    //资金流水
    @RequestMapping("/fundFlush/list")
    public String fundFlushList(Model model, HttpSession session, FundFlush obj){
        User user1 = (User) session.getAttribute("user");
        if(obj.getPageSize() == null){
            obj.setPageSize(15);
        }
        if(obj.getCurrentPage() == null ){
            obj.setCurrentPage(0);
        }
        if(obj.getFlag() == null){
            obj.setFlag(0);
        }
        obj.setUserId(user1.getId());
        obj.setCurrentPage(obj.getCurrentPage() + obj.getFlag());//控制翻页
        obj.setPageStart(obj.getCurrentPage() *  obj.getPageSize());
        model.addAttribute("page",fundFlushService.page(obj));
        return "user/fundFlushList";
    }


}
