package com.hs.tool;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
@Slf4j
public class HttpUtils {
//    https://blog.csdn.net/simon803/article/details/7784682
//    这个字符串由许多数据拼接在一起，不同含义的数据用逗号隔开了，按照程序员的思路，顺序号从0开始。
//            0：”大秦铁路”，股票名字；
//            1：”27.55″，今日开盘价；
//            2：”27.25″，昨日收盘价；
//            3：”26.91″，当前价格；
//            4：”27.55″，今日最高价；
//            5：”26.20″，今日最低价；
//            6：”26.91″，竞买价，即“买一”报价；
//            7：”26.92″，竞卖价，即“卖一”报价；
//            8：”22114263″，成交的股票数，由于股票交易以一百股为基本单位，所以在使用时，通常把该值除以一百；
//            9：”589824680″，成交金额，单位为“元”，为了一目了然，通常以“万元”为成交金额的单位，所以通常把该值除以一万；
//            10：”4695″，“买一”申请4695股，即47手；
//            11：”26.91″，“买一”报价；
//            12：”57590″，“买二”
//            13：”26.90″，“买二”
//            14：”14700″，“买三”
//            15：”26.89″，“买三”
//            16：”14300″，“买四”
//            17：”26.88″，“买四”
//            18：”15100″，“买五”
//            19：”26.87″，“买五”
//            20：”3100″，“卖一”申报3100股，即31手；
//            21：”26.92″，“卖一”报价
//            (22, 23), (24, 25), (26,27), (28, 29)分别为“卖二”至“卖四的情况”
//            30：”2008-01-11″，日期；
//            31：”15:05:32″，时间；
//    一个简单的JavaScript应用例子:

    public static void main(String[] args) throws Exception {
        String r = reptile("sh000001");
        String[] sr = r.split("=");
        log.info(sr[0]);
        log.info(sr[1]);
        String[] srr = sr[1].split(",");
        String name = srr[0];
        Double todayPlate = Double.valueOf(srr[1]);
        Double lastDayPlate = Double.valueOf(srr[2]);
        Double buyerPrice = Double.valueOf(srr[11]);
        Integer buyerAmount = Integer.valueOf(srr[10]);
        buyerAmount = buyerAmount / 100;
        buyerAmount = buyerAmount * 100;
        Double sellerPrice = Double.valueOf(srr[21]);
        Integer sellerAmount = Integer.valueOf(srr[20]);
        sellerAmount = sellerAmount / 100;
        sellerAmount = sellerAmount * 100;
        System.out.println(sellerPrice);


    }
//    http://hq.sinajs.cn/list=sh000001
//    http://hq.sinajs.cn/list=sh601006
    public static String reptile(String code)  throws Exception{
        //1. 打开浏览器 创建httpclient对象
        CloseableHttpClient httpClient = HttpClients.createDefault();
//2. 输入网址
        HttpGet httpGet = new HttpGet("http://hq.sinajs.cn/list="+code);
//3. 发送请求
        CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
//4. 响应结果
        HttpEntity httpEntity = httpResponse.getEntity();
//5. 解析结果
        String result = EntityUtils.toString(httpEntity, "utf-8");
        log.info(result);
        return result;
    }



}
