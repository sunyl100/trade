package com.hs.entity;

import lombok.Data;

import java.util.Date;
@Data
public class TradePlate {
    private Integer id;

    private Double price;

    private Date createTime;

}