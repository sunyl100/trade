package com.hs.entity;

import lombok.Data;

@Data
public class Good {
    private Integer id;

    private String name;

    private String desc;

    private Byte status;

}