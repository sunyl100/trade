package com.hs.entity;

import lombok.Data;

import java.util.Date;
@Data
public class Trade {


    private Integer currentPage; //

    private Integer pageSize;//页数

    private Integer pageStart ;//开始 limit

    private Integer flag ;//

    private String goodName;

    private String orderBy;




    private Integer id;

    private Byte type;

    private Integer userId;

    private String userName;

    private Integer goodId;

    private Integer initialAmount;

    private Integer amount;


    private Double price;

    private Date createTime;

    private Integer status;

    private Date updateTime;

}