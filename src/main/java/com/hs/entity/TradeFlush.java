package com.hs.entity;

import lombok.Data;

import java.util.Date;

@Data
public class TradeFlush {

    private Integer currentPage; //

    private Integer pageSize;//页数

    private Integer pageStart ;//开始 limit

    private Integer flag ;//

    private Integer limit ;//


    private Integer userId;

    private String goodName;

    private String buyerName;

    private String sellerName;



    private Integer id;

    private Integer buyer;

    private Integer seller;

    private Date createTime;

    private Integer amount;

    private Integer goodId;



    private Double price;

}