package com.hs.entity;

import com.hs.dao.OfferFlushMapper;
import com.hs.dao.OfferMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;

@Service
public class OfferFlushService {
    @Autowired
    OfferFlushMapper offerFlushMapper;
    @Autowired
    OfferMapper offerMapper;
    @RequestMapping("insert")
    @Transactional
    public int insert(OfferFlush offerFlush){
        offerFlush.setCreateTime(new Date());
        Offer offer = offerMapper.selectByPrimaryKey(offerFlush.getOfferId());
        if(offer == null){
            throw new RuntimeException("offer数据出错");
        }
        if(offer.getHighestPrice() == null || offer.getHighestPrice() < offerFlush.getPrice()){
            offer.setHighestPrice(offerFlush.getPrice());
            offer.setUpdateTime(new Date());
            if(offerMapper.updateByPrimaryKeySelective(offer) < 1){
                throw new RuntimeException("更新offer数据出错");
            }
        }
        return offerFlushMapper.insertSelective(offerFlush);
    }
    @Transactional
    @RequestMapping("editFlush")
    public int editFlush(OfferFlush offerFlush){
        Offer offer = offerMapper.selectByPrimaryKey(offerFlush.getOfferId());
        if(offer == null){
            throw new RuntimeException("offer数据出错");
        }
        if(offer.getHighestPrice() == null || offer.getHighestPrice() < offerFlush.getPrice()){
            offer.setHighestPrice(offerFlush.getPrice());
            offer.setUpdateTime(new Date());
            if(offerMapper.updateByPrimaryKeySelective(offer) < 1){
                throw new RuntimeException("更新offer数据出错");
            }
        }

        return offerFlushMapper.editFlush(offerFlush);
    }
    @RequestMapping("list")
    public List<OfferFlush> list(OfferFlush offerFlush){
        return offerFlushMapper.selectByCondition(offerFlush);
    }

}
