package com.hs.entity;

import lombok.Data;

import java.util.Date;

@Data

public class Offer  {
    private Integer currentPage; //

    private Integer pageSize;//页数

    private Integer pageStart ;//开始 limit

    private Integer flag ;//

    private Integer userId ;//



    private Integer id;

    private Integer goodId;

    private Integer limitTime;

    private Byte status;

    private Double initPrice;

    private String goodName;

    private Date createTime;

    private Double highestPrice;

    private Date updateTime;
}