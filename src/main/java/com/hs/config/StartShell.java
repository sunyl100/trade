package com.hs.config;

import com.hs.dao.TradeFlushMapper;
import com.hs.dao.TradePlateMapper;
import com.hs.entity.TradeFlush;
import com.hs.entity.TradePlate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 继承Application接口后项目启动时会按照执行顺序执行run方法
 * 通过设置Order的value来指定执行的顺序
 */
@Component
@Order(value = 1)
public class StartShell implements ApplicationRunner {

    @Autowired
    TradeFlushMapper tradeFlushMapper;
    @Autowired
    TradePlateMapper tradePlateMapper;

    @Override
    public void run(ApplicationArguments args) {
        TradeFlush tradeFlush = tradeFlushMapper.getTradeFlushLast();
        if(tradeFlush != null){
            MyValues.TRADE_PLATE_PRICE = tradeFlush.getPrice();
        }
        System.out.println(MyValues.TRADE_PLATE_PRICE);
    }


}