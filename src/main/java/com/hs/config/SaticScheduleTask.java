package com.hs.config;

import com.hs.dao.*;
import com.hs.entity.*;
import com.hs.service.TradeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
public class SaticScheduleTask {
    @Autowired
    OfferMapper offerMapper;
    @Autowired
    TradeMapper tradeMapper;
    @Autowired
    GoodMapper goodMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    UserGoodMapper userGoodMapper;
    @Autowired
    TradeService tradeService;
    @Autowired
    TradePlateMapper tradePlateMapper;
    @Autowired
    TradeFlushMapper tradeFlushMapper;

    //3.添加定时任务
    @Scheduled(cron = "0/10 * * * * ?")
    //或直接指定时间间隔，例如：5秒
    //@Scheduled(fixedRate=5000)
    private void configureTasks() {
        if(!MyValues.task_offer){
//            log.info("task_offer   ScheduleTask未开启");
            return;
        }
        int i = 0;
        Offer offer = new Offer();
        offer.setPageSize(null);
        offer.setStatus(new Integer(1).byteValue());
        List<Offer> offerList = offerMapper.selectByCondition(offer);
        for(Offer of : offerList){
            Long m = System.currentTimeMillis() -  of.getCreateTime().getTime();
            int cutTime =  new Long(m / (1000*60))   .intValue() - of.getLimitTime();
            if(cutTime >=  0){
                of.setStatus(new Integer(2).byteValue());
                int j = offerMapper.updateByPrimaryKey(of);
                i++;
            }
        }
        System.err.println("执行静态定时任务时间: " + LocalDateTime.now());
        System.out.println("更新"+i+"条");
    }

    @Scheduled(cron = "0/5 * * * * ?")
    //或直接指定时间间隔，例如：5秒
    private void configureTaskForTrade() throws Exception {
        if(MyValues.task_trade == false){
            return;
        }
        Long l = System.currentTimeMillis();
        tradeService.tradeSchedule();
        log.info("trade   ScheduleTask耗时："+(System.currentTimeMillis()-l)+"mm");

    }
    @Scheduled(cron = "0/1 * * * * ?")
    //或直接指定时间间隔，例如：1秒
    private void reptile() throws Exception {
//        if(MyValues.Task_PLATE_PRICE == false){
//            return;
//        }
        if(MyValues.reptile == false){
            return;
        }
        Long l = System.currentTimeMillis();
        tradeService.reptile();
        log.info("reptile   ScheduleTask耗时："+(System.currentTimeMillis()-l)+"mm");

    }
    @Scheduled(cron = "0 */30 * * * ?")
//    每隔30分钟执行一次
    private void configureTaskForTradePeriod() {
        if(MyValues.Task_PLATE_PRICE == false){
            return;
        }
        //  收盘
        TradeFlush tradeFlush = tradeFlushMapper.getTradeFlushLast();
        if(tradeFlush != null){
            TradePlate tradePlate = new TradePlate();
            tradePlate.setCreateTime(new Date());
            tradePlate.setPrice(tradeFlush.getPrice());
            tradePlateMapper.insertSelective(tradePlate);
            MyValues.TRADE_PLATE_PRICE = tradeFlush.getPrice();
        }
    }

    public static void main(String[] args) {
        List<User> list = new ArrayList<>();
        User user = new User();
        user.setId(1);
        user.setName("CC");
        list.add(user);
        for(User u : list){
            u.setName("XX");
        }
        System.out.println(list.toString());
    }
}