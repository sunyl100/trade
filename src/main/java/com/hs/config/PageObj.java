package com.hs.config;

import java.util.List;

/**
 * 展示页对象
 */
public class PageObj {
    private int currentPage; //总页数
    private int pageStart;//开始
    private int pageSize;//页数
    private int total; //总个数
    private int totalPage; //总页数
    private Object list;

    public PageObj(int currentPage, int pageSize, int total,Object list) {
        this.currentPage = currentPage;
        this.pageStart = currentPage * pageSize;
        this.pageSize = pageSize;
        this.total = total;
        int i = total/pageSize;
        int j = total % pageSize;

        this.totalPage = i+(j != 0 ? 1 : 0);
        this.list = list;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPageStart() {
        return pageStart;
    }

    public void setPageStart(int pageStart) {
        this.pageStart = pageStart;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public Object getList() {
        return list;
    }

    public void setList(Object list) {
        this.list = list;
    }

    public static void main(String[] args) {
        System.out.println(new Double(Math.ceil(19/15)).intValue());
        int i  =1/15;
        int j  =1 % 15;

        System.out.println(i +"--"+j);
        System.out.println(i+(j!=0?1:0));

    }
}
