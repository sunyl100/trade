package com.hs.config;

import lombok.Data;

@Data
public class R {

    private int code;
    private String msg;
    private Object data;

    public static String S = "成功";
    public static String F = "失败";

    public R(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static R SUCCESS(String msg, Object data) {
        return new R(200,msg,data);
    }
    public static R FAILURE(String msg, Object data) {
        return new R(201,msg,data);
    }





}
