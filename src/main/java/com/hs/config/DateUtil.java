package com.hs.config;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    public static String FormatDate(Date date){
        String strDateFormat = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);
        return sdf.format(date);

    }
}
