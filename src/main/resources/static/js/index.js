$(function () {
    line();
    listener();
});
function listener(){
    //监听左侧菜单栏被点击事件
    $(".p").click(function () {
        var par = document.getElementsByClassName("p");
        for(var i = 0;i < par.length;i++){
            $(par[i]).removeClass("pp");
        }
        $(this).addClass("pp");
        $(this).siblings().addClass("pp");
    });
}
//加载模块
function load(url) {
    //创建异步对象
    var xhr = new XMLHttpRequest();
    //设置请求的类型及url
    xhr.open('get', url,true);
    //post请求一定要添加请求头才行不然会报错
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    //发送请求
    xhr.send("");
    xhr.onreadystatechange = function () {
        // 这步为判断服务器是否正确响应
        if (xhr.readyState == 4 && xhr.status == 200) {
            // console.log(xhr.responseText);
            document.getElementById("content").innerHTML = xhr.responseText;
        }
    };
}
//访问页面完成操作并跳转页面
function sendJump(url,jumpUrl,da,formId) {
    //创建异步对象
    var xhr = new XMLHttpRequest();
    //设置请求的类型及url
    xhr.open('post', url,true);
    //post请求一定要添加请求头才行不然会报错
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    //发送请求
    var data = $("#form").serialize();
    if(formId){
        data = $('#'+formId).serialize();
    }
    if(da){
        data = da;
    }
    console.log(data);
    xhr.send(data);
    xhr.onreadystatechange = function () {
        // 这步为判断服务器是否正确响应
        if (xhr.readyState == 4 && xhr.status == 200) {
            console.log(xhr.responseText);
            var re = JSON.parse(xhr.responseText);
            if(re.code == 200 ){

                if(jumpUrl == '/'){
                    window.location.href = '/';
                    return;
                }
                if(url.indexOf("insert")!= -1){
                    load(jumpUrl);
                }else{
                    new Dialog().alert('操作成功！', {
                        type: 'success'
                    });
                    setTimeout(load(jumpUrl),2000);
                }

            }else{
                new Dialog().alert(re.msg, {
                    type: 'failure'
                });
            }
        }
    };
}
//分页跳转
function loadPage(url,flag) {
    //赋值flag
    document.getElementById("flag").value = flag;
    //创建异步对象
    var xhr = new XMLHttpRequest();
    //设置请求的类型及url
    xhr.open('post', url,true);
    //post请求一定要添加请求头才行不然会报错
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    //发送请求
    var data = $("#form").serialize();
    console.log(data);
    xhr.send(data);
    xhr.onreadystatechange = function () {
        // 这步为判断服务器是否正确响应
        if (xhr.readyState == 4 && xhr.status == 200) {
            document.getElementById("content").innerHTML = xhr.responseText;
        }
    };
}
//业务 -- 添加页面 String  基于sendJump
function baojia(offerId) {
    new Dialog({
        title: '报价',
        content: '<h6>请填写报价(单位：元)</h6>' +
            '<p style="margin-top: 50px;"><input style="font-size: 18px;" id="price" type="number" class="ui-input"></p>',
        buttons: [{
            events: function (event) {
                // 按钮禁用
                // event.dialog是当前实例对象
                console.log(event)
                console.log(event.type)
                if(event.type){
                    console.log(document.getElementById("price").value);
                }
                var data = "";
                data = data + "price="+document.getElementById("price").value;
                data = data + "&offerId="+offerId;
                console.log(data);
                sendJump("/offerFlush/insert","/offer/offerWithFlush",data);
                event.dialog.remove();
            }
        }, {}]
    });
}
//业务 --  html    jumpDialog
function checkbaojia(offerId) {
    var data = "offerId="+offerId;
    this.jumpDialog("/offer/offerFlushDetail",data,'报价');
}
function jumpDialog(url,data,title){
    var html = "";
    //创建异步对象
    var xhr = new XMLHttpRequest();
    //设置请求的类型及url
    xhr.open('post', url,true);
    //post请求一定要添加请求头才行不然会报错
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    //发送请求
    var data = data;
    // if(data!=null &&data){
    xhr.send(data);
    // }
    xhr.onreadystatechange = function () {
        // 这步为判断服务器是否正确响应
        if (xhr.readyState == 4 && xhr.status == 200) {
            console.log( xhr.responseText);
            html = xhr.responseText;
            if(url.indexOf("add") != -1 || url.indexOf("Add") != -1 || url.indexOf("edit") != -1 ||  url.indexOf("Edit") != -1){
                var a = new Dialog({
                    title: title,
                    content: html,
                    buttons: [{
                        //有操作
                        events: function (event) {
                            document.getElementById("button").click();
                            // 按钮禁用
                            // event.dialog是当前实例对象
                            event.dialog.remove();
                        }

                    }, {}]
                });
            }else{
                new Dialog({
                    title: title,
                    content: html,
                    buttons: [{
                    }, {}]
                });
            }

        }
    };

}
//  编辑  String  基于sendJump
function baojiaEdit(offerId,price) {
    new Dialog({
        title: '报价',
        content: '<h6>请填写报价(单位：元)</h6>' +
            '<p style="margin-top: 50px;"><input style="font-size: 18px;" id="price" placeholder = '+price+' type="number" class="ui-input"></p>',
        buttons: [{
            events: function (event) {
                // 按钮禁用
                // event.dialog是当前实例对象
                console.log(event)
                console.log(event.type)
                if(event.type){
                    console.log(document.getElementById("price").value);
                }
                var data = "";
                data = data + "price="+document.getElementById("price").value;
                data = data + "&offerId="+offerId;
                console.log(data);
                sendJump("/offerFlush/edit","/offer/offerWithFlush",data);
                event.dialog.remove();
            }
        }, {}]
    });
}
//  编辑  String  基于sendJump
function userEdit(userId,fund) {
    new Dialog({
        title: '资金修改',
        content: '<h6>请填写用户资金(单位：元)</h6>' +
            '<p style="margin-top: 50px;"><input style="font-size: 18px;" id="fund" placeholder = '+fund+' type="number" class="ui-input"></p>',
        buttons: [{
            events: function (event) {
                // 按钮禁用
                // event.dialog是当前实例对象
                console.log(event)
                console.log(event.type)
                if(event.type){
                    console.log(document.getElementById("fund").value);
                }
                var data = "";
                data = data + "fund="+document.getElementById("fund").value;
                data = data + "&id="+userId;
                console.log(data);
                sendJump("/user/edit","/user/list",data);
                event.dialog.remove();
            }
        }, {}]
    });
}
//  编辑  String  基于sendJump
function tradePlatePriceEdit(price) {
    new Dialog({
        title: '收盘价',
        content: '<h6>请填写(单位：元)</h6>' +
            '<p style="margin-top: 50px;"><input style="font-size: 18px;" id="price" placeholder = '+price+' type="number" class="ui-input"></p>',
        buttons: [{
            events: function (event) {
                // 按钮禁用
                // event.dialog是当前实例对象
                console.log(event)
                console.log(event.type)
                if(event.type){
                    console.log(document.getElementById("price").value);
                }
                var data = "";
                data = data + "price="+document.getElementById("price").value;
                // data = data + "&id="+userId;
                console.log(data);
                sendJump("/tradePlatePriceEdit","/",data);
                event.dialog.remove();
            }
        }, {}]
    });
}
function excelExport(url){
    if(document.getElementById("offerId")){
        var offerId = document.getElementById("offerId").value;
        window.location.href = url+"?offerId="+offerId;
    }else{
        window.location.href = url;
    }
}
function task(id,url) {
    if(document.getElementById(id).checked){
        load(url+"?flag=1")
    }else{
        load(url+"?flag=0")
    }

}
function crtTimeFtt(val) {
    if (val != null) {
        var date = new Date(val);
        return date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate() + " "+ date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
    }
}
function line(_this){
    var limit = $(_this).val();
    console.log(limit);
    var xhr = new XMLHttpRequest();
    xhr.open('post', '/tradeFlush/tradeFlushLineData',true);
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    var data = "limit=30";
    if(limit){
        data = "limit="+limit;
    }
    xhr.send(data);
    xhr.onreadystatechange = function () {
        // 这步为判断服务器是否正确响应
        if (xhr.readyState == 4 && xhr.status == 200) {
            var re = JSON.parse(xhr.responseText);
            if(re.code == 200){
                console.log(re);
                var list = re.data;
                var dom = document.getElementById("container");
                var myChart = echarts.init(dom);
                // var option_data = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
                var option_data = [];//['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
                var series_data = [];//[150, 230, 224, 218, 135, 147, 260];
                var j = 0;
                for(var i = list.length - 1 ;i >= 0 ;i --){
                    option_data[j] = crtTimeFtt(list[i].createTime);
                    series_data[j] = list[i].price;
                    j ++;
                }
                console.log(option_data);
                console.log(series_data);
                var option = {
                    xAxis: {
                        type: 'category',
                        data: option_data
                    },
                    yAxis: {
                        type: 'value'
                    },
                    series: [{
                        data: series_data,
                        type: 'line'
                    }]
                };

                if (option && typeof option === 'object') {
                    myChart.setOption(option);
                }

            }

        }
    };





}
