/*
Navicat MySQL Data Transfer

Source Server         : 192.168.1.141
Source Server Version : 50726
Source Host           : 192.168.1.141:3306
Source Database       : trade

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2021-03-19 13:46:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bus_fund_flush
-- ----------------------------
DROP TABLE IF EXISTS `bus_fund_flush`;
CREATE TABLE `bus_fund_flush` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `fund` double(255,0) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of bus_fund_flush
-- ----------------------------
INSERT INTO `bus_fund_flush` VALUES ('5', '47', '-100', '2021-03-18 16:57:00', '买入物品（1）：100单位');
INSERT INTO `bus_fund_flush` VALUES ('6', '1', '100', '2021-03-18 16:57:00', '卖出入物品（1）100单位');
INSERT INTO `bus_fund_flush` VALUES ('7', '48', '-100', '2021-03-18 16:57:00', '买入物品（1）100单位');
INSERT INTO `bus_fund_flush` VALUES ('8', '1', '100', '2021-03-18 16:57:00', '卖出入物品100单位');
INSERT INTO `bus_fund_flush` VALUES ('9', '47', '-100', '2021-03-18 17:06:25', '买入物品(key=1):100单位');
INSERT INTO `bus_fund_flush` VALUES ('10', '48', '100', '2021-03-18 17:06:25', '卖出入物品(key=1):100单位');

-- ----------------------------
-- Table structure for bus_offer
-- ----------------------------
DROP TABLE IF EXISTS `bus_offer`;
CREATE TABLE `bus_offer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `good_id` int(11) DEFAULT NULL,
  `good_name` varchar(255) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `limit_time` int(10) DEFAULT NULL COMMENT '拍卖结束限时（分钟）',
  `init_price` double DEFAULT '0' COMMENT '初始价格',
  `status` tinyint(2) DEFAULT '1' COMMENT '0未开始 1 进行中 2 已 结束',
  `create_time` datetime DEFAULT NULL,
  `highest_price` double DEFAULT '0',
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci COMMENT='物品拍卖表';

-- ----------------------------
-- Records of bus_offer
-- ----------------------------
INSERT INTO `bus_offer` VALUES ('16', '1', '平安证券', '1', '1', '2', '2021-03-18 17:09:40', '5', '2021-03-18 17:10:30');
INSERT INTO `bus_offer` VALUES ('17', '1', null, '2', '2', '1', '2021-03-19 11:39:26', '1', '2021-03-19 11:41:51');
INSERT INTO `bus_offer` VALUES ('18', '1', null, '3', '1', '1', '2021-03-19 11:41:45', '3', '2021-03-19 11:42:07');

-- ----------------------------
-- Table structure for bus_offer_flush
-- ----------------------------
DROP TABLE IF EXISTS `bus_offer_flush`;
CREATE TABLE `bus_offer_flush` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `offer_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL COMMENT '报价',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `offer_id` (`offer_id`,`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci COMMENT='物品拍卖的出价流水';

-- ----------------------------
-- Records of bus_offer_flush
-- ----------------------------
INSERT INTO `bus_offer_flush` VALUES ('31', '16', '47', '3', '2021-03-18 17:09:56');
INSERT INTO `bus_offer_flush` VALUES ('32', '16', '48', '5', '2021-03-18 17:10:15');
INSERT INTO `bus_offer_flush` VALUES ('33', '17', '1', '1', '2021-03-19 11:41:51');
INSERT INTO `bus_offer_flush` VALUES ('34', '18', '47', '3', '2021-03-19 11:42:07');

-- ----------------------------
-- Table structure for bus_trade
-- ----------------------------
DROP TABLE IF EXISTS `bus_trade`;
CREATE TABLE `bus_trade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(2) DEFAULT NULL COMMENT '1买 2 卖',
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `good_id` int(11) DEFAULT NULL COMMENT '暂时不用',
  `initial_amount` int(255) DEFAULT NULL COMMENT '初始交易数量',
  `amount` int(255) DEFAULT '0' COMMENT '数量',
  `price` double DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `status` int(2) DEFAULT '0' COMMENT '0未完成 1已完成',
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

-- ----------------------------
-- Records of bus_trade
-- ----------------------------
INSERT INTO `bus_trade` VALUES ('15', '1', '47', '1', '1', '100', '100', '1', '2021-03-18 16:48:21', '1', '2021-03-18 16:57:00');
INSERT INTO `bus_trade` VALUES ('16', '1', '48', '2', '1', '100', '100', '1', '2021-03-18 16:48:39', '1', '2021-03-18 16:57:00');
INSERT INTO `bus_trade` VALUES ('17', '2', '1', 'admin', '1', '200', '100', '1', '2021-03-18 16:49:35', '1', '2021-03-18 16:57:00');
INSERT INTO `bus_trade` VALUES ('18', '2', '48', '2', '1', '100', '100', '1', '2021-03-18 17:04:48', '1', '2021-03-18 17:06:25');
INSERT INTO `bus_trade` VALUES ('19', '1', '47', '1', '1', '100', '100', '1.1', '2021-03-18 17:05:59', '1', '2021-03-18 17:06:25');
INSERT INTO `bus_trade` VALUES ('20', '2', '47', '1', '1', '100', '100', '1', '2021-03-19 11:21:37', '0', null);

-- ----------------------------
-- Table structure for bus_trade_flush
-- ----------------------------
DROP TABLE IF EXISTS `bus_trade_flush`;
CREATE TABLE `bus_trade_flush` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `good_id` int(11) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL COMMENT '交易的数量',
  `price` double DEFAULT NULL,
  `buyer` int(255) DEFAULT NULL COMMENT '买方',
  `seller` int(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

-- ----------------------------
-- Records of bus_trade_flush
-- ----------------------------
INSERT INTO `bus_trade_flush` VALUES ('72', '1', '100', '1', '47', '1', '2021-03-18 16:57:00');
INSERT INTO `bus_trade_flush` VALUES ('73', '1', '100', '1', '48', '1', '2021-03-18 16:57:00');
INSERT INTO `bus_trade_flush` VALUES ('74', '1', '100', '1', '47', '48', '2021-03-18 17:06:25');

-- ----------------------------
-- Table structure for bus_trade_plate
-- ----------------------------
DROP TABLE IF EXISTS `bus_trade_plate`;
CREATE TABLE `bus_trade_plate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` double DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=635 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of bus_trade_plate
-- ----------------------------
INSERT INTO `bus_trade_plate` VALUES ('1', '1', null);
INSERT INTO `bus_trade_plate` VALUES ('141', '1', '2021-03-18 16:58:00');
INSERT INTO `bus_trade_plate` VALUES ('142', '1', '2021-03-18 16:59:00');
INSERT INTO `bus_trade_plate` VALUES ('143', '1', '2021-03-18 17:00:00');
INSERT INTO `bus_trade_plate` VALUES ('144', '1', '2021-03-18 17:01:00');
INSERT INTO `bus_trade_plate` VALUES ('145', '1', '2021-03-18 17:02:00');
INSERT INTO `bus_trade_plate` VALUES ('146', '1', '2021-03-18 17:03:00');
INSERT INTO `bus_trade_plate` VALUES ('147', '1', '2021-03-18 17:04:00');
INSERT INTO `bus_trade_plate` VALUES ('148', '1', '2021-03-18 17:05:00');
INSERT INTO `bus_trade_plate` VALUES ('149', '1', '2021-03-18 17:06:00');
INSERT INTO `bus_trade_plate` VALUES ('150', '1', '2021-03-18 17:07:00');
INSERT INTO `bus_trade_plate` VALUES ('151', '1', '2021-03-18 17:08:00');
INSERT INTO `bus_trade_plate` VALUES ('152', '1', '2021-03-18 17:09:00');
INSERT INTO `bus_trade_plate` VALUES ('153', '1', '2021-03-18 17:10:00');
INSERT INTO `bus_trade_plate` VALUES ('154', '1', '2021-03-18 17:11:00');
INSERT INTO `bus_trade_plate` VALUES ('155', '1', '2021-03-18 17:12:00');
INSERT INTO `bus_trade_plate` VALUES ('156', '1', '2021-03-18 17:13:00');
INSERT INTO `bus_trade_plate` VALUES ('157', '1', '2021-03-18 17:14:00');
INSERT INTO `bus_trade_plate` VALUES ('158', '1', '2021-03-18 17:15:00');
INSERT INTO `bus_trade_plate` VALUES ('159', '1', '2021-03-18 17:16:00');
INSERT INTO `bus_trade_plate` VALUES ('160', '1', '2021-03-18 17:17:00');
INSERT INTO `bus_trade_plate` VALUES ('161', '1', '2021-03-18 17:18:00');
INSERT INTO `bus_trade_plate` VALUES ('162', '1', '2021-03-18 17:19:00');
INSERT INTO `bus_trade_plate` VALUES ('163', '1', '2021-03-18 17:20:00');
INSERT INTO `bus_trade_plate` VALUES ('164', '1', '2021-03-18 17:21:00');
INSERT INTO `bus_trade_plate` VALUES ('165', '1', '2021-03-18 17:22:00');
INSERT INTO `bus_trade_plate` VALUES ('166', '1', '2021-03-18 17:23:00');
INSERT INTO `bus_trade_plate` VALUES ('167', '1', '2021-03-18 17:24:00');
INSERT INTO `bus_trade_plate` VALUES ('168', '1', '2021-03-18 17:25:00');
INSERT INTO `bus_trade_plate` VALUES ('169', '1', '2021-03-18 17:26:00');
INSERT INTO `bus_trade_plate` VALUES ('170', '1', '2021-03-18 17:27:00');
INSERT INTO `bus_trade_plate` VALUES ('171', '1', '2021-03-18 17:28:00');
INSERT INTO `bus_trade_plate` VALUES ('172', '1', '2021-03-18 17:29:00');
INSERT INTO `bus_trade_plate` VALUES ('173', '1', '2021-03-18 17:30:00');
INSERT INTO `bus_trade_plate` VALUES ('174', '1', '2021-03-18 17:31:00');
INSERT INTO `bus_trade_plate` VALUES ('175', '1', '2021-03-18 17:32:00');
INSERT INTO `bus_trade_plate` VALUES ('176', '1', '2021-03-18 17:33:00');
INSERT INTO `bus_trade_plate` VALUES ('177', '1', '2021-03-19 08:43:00');
INSERT INTO `bus_trade_plate` VALUES ('178', '1', '2021-03-19 08:44:00');
INSERT INTO `bus_trade_plate` VALUES ('179', '1', '2021-03-19 08:45:00');
INSERT INTO `bus_trade_plate` VALUES ('180', '1', '2021-03-19 08:46:00');
INSERT INTO `bus_trade_plate` VALUES ('181', '1', '2021-03-19 08:47:00');
INSERT INTO `bus_trade_plate` VALUES ('182', '1', '2021-03-19 08:48:00');
INSERT INTO `bus_trade_plate` VALUES ('183', '1', '2021-03-19 08:49:00');
INSERT INTO `bus_trade_plate` VALUES ('184', '1', '2021-03-19 08:50:00');
INSERT INTO `bus_trade_plate` VALUES ('185', '1', '2021-03-19 08:51:00');
INSERT INTO `bus_trade_plate` VALUES ('186', '1', '2021-03-19 08:52:00');
INSERT INTO `bus_trade_plate` VALUES ('187', '1', '2021-03-19 08:53:00');
INSERT INTO `bus_trade_plate` VALUES ('188', '1', '2021-03-19 08:54:00');
INSERT INTO `bus_trade_plate` VALUES ('189', '1', '2021-03-19 08:55:00');
INSERT INTO `bus_trade_plate` VALUES ('190', '1', '2021-03-19 08:56:00');
INSERT INTO `bus_trade_plate` VALUES ('191', '1', '2021-03-19 08:57:00');
INSERT INTO `bus_trade_plate` VALUES ('192', '1', '2021-03-19 08:58:00');
INSERT INTO `bus_trade_plate` VALUES ('193', '1', '2021-03-19 08:59:00');
INSERT INTO `bus_trade_plate` VALUES ('194', '1', '2021-03-19 09:00:00');
INSERT INTO `bus_trade_plate` VALUES ('195', '1', '2021-03-19 09:01:00');
INSERT INTO `bus_trade_plate` VALUES ('196', '1', '2021-03-19 09:02:00');
INSERT INTO `bus_trade_plate` VALUES ('197', '1', '2021-03-19 09:03:00');
INSERT INTO `bus_trade_plate` VALUES ('198', '1', '2021-03-19 09:04:00');
INSERT INTO `bus_trade_plate` VALUES ('199', '1', '2021-03-19 09:05:00');
INSERT INTO `bus_trade_plate` VALUES ('200', '1', '2021-03-19 09:06:00');
INSERT INTO `bus_trade_plate` VALUES ('201', '1', '2021-03-19 09:07:00');
INSERT INTO `bus_trade_plate` VALUES ('202', '1', '2021-03-19 09:08:00');
INSERT INTO `bus_trade_plate` VALUES ('203', '1', '2021-03-19 09:09:00');
INSERT INTO `bus_trade_plate` VALUES ('204', '1', '2021-03-19 09:10:00');
INSERT INTO `bus_trade_plate` VALUES ('205', '1', '2021-03-19 09:11:00');
INSERT INTO `bus_trade_plate` VALUES ('206', '1', '2021-03-19 09:12:00');
INSERT INTO `bus_trade_plate` VALUES ('207', '1', '2021-03-19 09:13:00');
INSERT INTO `bus_trade_plate` VALUES ('208', '1', '2021-03-19 09:14:00');
INSERT INTO `bus_trade_plate` VALUES ('209', '1', '2021-03-19 09:15:00');
INSERT INTO `bus_trade_plate` VALUES ('210', '1', '2021-03-19 09:16:00');
INSERT INTO `bus_trade_plate` VALUES ('211', '1', '2021-03-19 09:17:00');
INSERT INTO `bus_trade_plate` VALUES ('212', '1', '2021-03-19 09:18:00');
INSERT INTO `bus_trade_plate` VALUES ('213', '1', '2021-03-19 09:19:00');
INSERT INTO `bus_trade_plate` VALUES ('214', '1', '2021-03-19 09:20:00');
INSERT INTO `bus_trade_plate` VALUES ('215', '1', '2021-03-19 09:21:00');
INSERT INTO `bus_trade_plate` VALUES ('216', '1', '2021-03-19 09:22:00');
INSERT INTO `bus_trade_plate` VALUES ('217', '1', '2021-03-19 09:23:00');
INSERT INTO `bus_trade_plate` VALUES ('218', '1', '2021-03-19 09:24:00');
INSERT INTO `bus_trade_plate` VALUES ('219', '1', '2021-03-19 09:25:00');
INSERT INTO `bus_trade_plate` VALUES ('220', '1', '2021-03-19 09:26:00');
INSERT INTO `bus_trade_plate` VALUES ('221', '1', '2021-03-19 09:27:00');
INSERT INTO `bus_trade_plate` VALUES ('222', '1', '2021-03-19 09:28:00');
INSERT INTO `bus_trade_plate` VALUES ('223', '1', '2021-03-19 09:28:00');
INSERT INTO `bus_trade_plate` VALUES ('224', '1', '2021-03-19 09:29:00');
INSERT INTO `bus_trade_plate` VALUES ('225', '1', '2021-03-19 09:29:00');
INSERT INTO `bus_trade_plate` VALUES ('226', '1', '2021-03-19 09:30:00');
INSERT INTO `bus_trade_plate` VALUES ('227', '1', '2021-03-19 09:30:00');
INSERT INTO `bus_trade_plate` VALUES ('228', '1', '2021-03-19 09:31:00');
INSERT INTO `bus_trade_plate` VALUES ('229', '1', '2021-03-19 09:31:00');
INSERT INTO `bus_trade_plate` VALUES ('230', '1', '2021-03-19 09:32:00');
INSERT INTO `bus_trade_plate` VALUES ('231', '1', '2021-03-19 09:32:00');
INSERT INTO `bus_trade_plate` VALUES ('232', '1', '2021-03-19 09:33:00');
INSERT INTO `bus_trade_plate` VALUES ('233', '1', '2021-03-19 09:33:00');
INSERT INTO `bus_trade_plate` VALUES ('234', '1', '2021-03-19 09:34:00');
INSERT INTO `bus_trade_plate` VALUES ('235', '1', '2021-03-19 09:34:00');
INSERT INTO `bus_trade_plate` VALUES ('236', '1', '2021-03-19 09:35:00');
INSERT INTO `bus_trade_plate` VALUES ('237', '1', '2021-03-19 09:35:00');
INSERT INTO `bus_trade_plate` VALUES ('238', '1', '2021-03-19 09:36:00');
INSERT INTO `bus_trade_plate` VALUES ('239', '1', '2021-03-19 09:36:00');
INSERT INTO `bus_trade_plate` VALUES ('240', '1', '2021-03-19 09:37:00');
INSERT INTO `bus_trade_plate` VALUES ('241', '1', '2021-03-19 09:37:00');
INSERT INTO `bus_trade_plate` VALUES ('242', '1', '2021-03-19 09:38:00');
INSERT INTO `bus_trade_plate` VALUES ('243', '1', '2021-03-19 09:38:00');
INSERT INTO `bus_trade_plate` VALUES ('244', '1', '2021-03-19 09:39:00');
INSERT INTO `bus_trade_plate` VALUES ('245', '1', '2021-03-19 09:39:00');
INSERT INTO `bus_trade_plate` VALUES ('246', '1', '2021-03-19 09:40:00');
INSERT INTO `bus_trade_plate` VALUES ('247', '1', '2021-03-19 09:40:00');
INSERT INTO `bus_trade_plate` VALUES ('248', '1', '2021-03-19 09:41:00');
INSERT INTO `bus_trade_plate` VALUES ('249', '1', '2021-03-19 09:41:00');
INSERT INTO `bus_trade_plate` VALUES ('250', '1', '2021-03-19 09:42:00');
INSERT INTO `bus_trade_plate` VALUES ('251', '1', '2021-03-19 09:42:00');
INSERT INTO `bus_trade_plate` VALUES ('252', '1', '2021-03-19 09:43:00');
INSERT INTO `bus_trade_plate` VALUES ('253', '1', '2021-03-19 09:43:00');
INSERT INTO `bus_trade_plate` VALUES ('254', '1', '2021-03-19 09:44:00');
INSERT INTO `bus_trade_plate` VALUES ('255', '1', '2021-03-19 09:44:00');
INSERT INTO `bus_trade_plate` VALUES ('256', '1', '2021-03-19 09:45:00');
INSERT INTO `bus_trade_plate` VALUES ('257', '1', '2021-03-19 09:45:00');
INSERT INTO `bus_trade_plate` VALUES ('258', '1', '2021-03-19 09:46:00');
INSERT INTO `bus_trade_plate` VALUES ('259', '1', '2021-03-19 09:46:00');
INSERT INTO `bus_trade_plate` VALUES ('260', '1', '2021-03-19 09:47:00');
INSERT INTO `bus_trade_plate` VALUES ('261', '1', '2021-03-19 09:47:00');
INSERT INTO `bus_trade_plate` VALUES ('262', '1', '2021-03-19 09:48:00');
INSERT INTO `bus_trade_plate` VALUES ('263', '1', '2021-03-19 09:48:00');
INSERT INTO `bus_trade_plate` VALUES ('264', '1', '2021-03-19 09:49:00');
INSERT INTO `bus_trade_plate` VALUES ('265', '1', '2021-03-19 09:49:00');
INSERT INTO `bus_trade_plate` VALUES ('266', '1', '2021-03-19 09:50:00');
INSERT INTO `bus_trade_plate` VALUES ('267', '1', '2021-03-19 09:50:00');
INSERT INTO `bus_trade_plate` VALUES ('268', '1', '2021-03-19 09:51:00');
INSERT INTO `bus_trade_plate` VALUES ('269', '1', '2021-03-19 09:51:00');
INSERT INTO `bus_trade_plate` VALUES ('270', '1', '2021-03-19 09:52:00');
INSERT INTO `bus_trade_plate` VALUES ('271', '1', '2021-03-19 09:52:00');
INSERT INTO `bus_trade_plate` VALUES ('272', '1', '2021-03-19 09:53:00');
INSERT INTO `bus_trade_plate` VALUES ('273', '1', '2021-03-19 09:53:00');
INSERT INTO `bus_trade_plate` VALUES ('274', '1', '2021-03-19 09:54:00');
INSERT INTO `bus_trade_plate` VALUES ('275', '1', '2021-03-19 09:54:00');
INSERT INTO `bus_trade_plate` VALUES ('276', '1', '2021-03-19 09:55:00');
INSERT INTO `bus_trade_plate` VALUES ('277', '1', '2021-03-19 09:55:00');
INSERT INTO `bus_trade_plate` VALUES ('278', '1', '2021-03-19 09:56:00');
INSERT INTO `bus_trade_plate` VALUES ('279', '1', '2021-03-19 09:56:00');
INSERT INTO `bus_trade_plate` VALUES ('280', '1', '2021-03-19 09:57:00');
INSERT INTO `bus_trade_plate` VALUES ('281', '1', '2021-03-19 09:57:00');
INSERT INTO `bus_trade_plate` VALUES ('282', '1', '2021-03-19 09:58:00');
INSERT INTO `bus_trade_plate` VALUES ('283', '1', '2021-03-19 09:58:00');
INSERT INTO `bus_trade_plate` VALUES ('284', '1', '2021-03-19 09:59:00');
INSERT INTO `bus_trade_plate` VALUES ('285', '1', '2021-03-19 09:59:00');
INSERT INTO `bus_trade_plate` VALUES ('286', '1', '2021-03-19 10:00:00');
INSERT INTO `bus_trade_plate` VALUES ('287', '1', '2021-03-19 10:00:00');
INSERT INTO `bus_trade_plate` VALUES ('288', '1', '2021-03-19 10:01:00');
INSERT INTO `bus_trade_plate` VALUES ('289', '1', '2021-03-19 10:01:00');
INSERT INTO `bus_trade_plate` VALUES ('290', '1', '2021-03-19 10:02:00');
INSERT INTO `bus_trade_plate` VALUES ('291', '1', '2021-03-19 10:02:00');
INSERT INTO `bus_trade_plate` VALUES ('292', '1', '2021-03-19 10:03:00');
INSERT INTO `bus_trade_plate` VALUES ('293', '1', '2021-03-19 10:03:00');
INSERT INTO `bus_trade_plate` VALUES ('294', '1', '2021-03-19 10:04:00');
INSERT INTO `bus_trade_plate` VALUES ('295', '1', '2021-03-19 10:04:00');
INSERT INTO `bus_trade_plate` VALUES ('296', '1', '2021-03-19 10:05:00');
INSERT INTO `bus_trade_plate` VALUES ('297', '1', '2021-03-19 10:05:00');
INSERT INTO `bus_trade_plate` VALUES ('298', '1', '2021-03-19 10:06:00');
INSERT INTO `bus_trade_plate` VALUES ('299', '1', '2021-03-19 10:06:00');
INSERT INTO `bus_trade_plate` VALUES ('300', '1', '2021-03-19 10:07:00');
INSERT INTO `bus_trade_plate` VALUES ('301', '1', '2021-03-19 10:07:00');
INSERT INTO `bus_trade_plate` VALUES ('302', '1', '2021-03-19 10:08:00');
INSERT INTO `bus_trade_plate` VALUES ('303', '1', '2021-03-19 10:08:00');
INSERT INTO `bus_trade_plate` VALUES ('304', '1', '2021-03-19 10:09:00');
INSERT INTO `bus_trade_plate` VALUES ('305', '1', '2021-03-19 10:09:00');
INSERT INTO `bus_trade_plate` VALUES ('306', '1', '2021-03-19 10:10:00');
INSERT INTO `bus_trade_plate` VALUES ('307', '1', '2021-03-19 10:10:00');
INSERT INTO `bus_trade_plate` VALUES ('308', '1', '2021-03-19 10:11:00');
INSERT INTO `bus_trade_plate` VALUES ('309', '1', '2021-03-19 10:11:00');
INSERT INTO `bus_trade_plate` VALUES ('310', '1', '2021-03-19 10:12:00');
INSERT INTO `bus_trade_plate` VALUES ('311', '1', '2021-03-19 10:12:00');
INSERT INTO `bus_trade_plate` VALUES ('312', '1', '2021-03-19 10:13:00');
INSERT INTO `bus_trade_plate` VALUES ('313', '1', '2021-03-19 10:13:00');
INSERT INTO `bus_trade_plate` VALUES ('314', '1', '2021-03-19 10:14:00');
INSERT INTO `bus_trade_plate` VALUES ('315', '1', '2021-03-19 10:14:00');
INSERT INTO `bus_trade_plate` VALUES ('316', '1', '2021-03-19 10:15:00');
INSERT INTO `bus_trade_plate` VALUES ('317', '1', '2021-03-19 10:15:00');
INSERT INTO `bus_trade_plate` VALUES ('318', '1', '2021-03-19 10:16:00');
INSERT INTO `bus_trade_plate` VALUES ('319', '1', '2021-03-19 10:16:00');
INSERT INTO `bus_trade_plate` VALUES ('320', '1', '2021-03-19 10:17:00');
INSERT INTO `bus_trade_plate` VALUES ('321', '1', '2021-03-19 10:17:00');
INSERT INTO `bus_trade_plate` VALUES ('322', '1', '2021-03-19 10:18:00');
INSERT INTO `bus_trade_plate` VALUES ('323', '1', '2021-03-19 10:18:00');
INSERT INTO `bus_trade_plate` VALUES ('324', '1', '2021-03-19 10:19:00');
INSERT INTO `bus_trade_plate` VALUES ('325', '1', '2021-03-19 10:19:00');
INSERT INTO `bus_trade_plate` VALUES ('326', '1', '2021-03-19 10:20:00');
INSERT INTO `bus_trade_plate` VALUES ('327', '1', '2021-03-19 10:20:00');
INSERT INTO `bus_trade_plate` VALUES ('328', '1', '2021-03-19 10:21:00');
INSERT INTO `bus_trade_plate` VALUES ('329', '1', '2021-03-19 10:21:00');
INSERT INTO `bus_trade_plate` VALUES ('330', '1', '2021-03-19 10:22:00');
INSERT INTO `bus_trade_plate` VALUES ('331', '1', '2021-03-19 10:22:00');
INSERT INTO `bus_trade_plate` VALUES ('332', '1', '2021-03-19 10:23:00');
INSERT INTO `bus_trade_plate` VALUES ('333', '1', '2021-03-19 10:23:00');
INSERT INTO `bus_trade_plate` VALUES ('334', '1', '2021-03-19 10:24:00');
INSERT INTO `bus_trade_plate` VALUES ('335', '1', '2021-03-19 10:24:00');
INSERT INTO `bus_trade_plate` VALUES ('336', '1', '2021-03-19 10:25:00');
INSERT INTO `bus_trade_plate` VALUES ('337', '1', '2021-03-19 10:25:00');
INSERT INTO `bus_trade_plate` VALUES ('338', '1', '2021-03-19 10:26:00');
INSERT INTO `bus_trade_plate` VALUES ('339', '1', '2021-03-19 10:26:00');
INSERT INTO `bus_trade_plate` VALUES ('340', '1', '2021-03-19 10:27:00');
INSERT INTO `bus_trade_plate` VALUES ('341', '1', '2021-03-19 10:27:00');
INSERT INTO `bus_trade_plate` VALUES ('342', '1', '2021-03-19 10:28:00');
INSERT INTO `bus_trade_plate` VALUES ('343', '1', '2021-03-19 10:28:00');
INSERT INTO `bus_trade_plate` VALUES ('344', '1', '2021-03-19 10:29:00');
INSERT INTO `bus_trade_plate` VALUES ('345', '1', '2021-03-19 10:29:00');
INSERT INTO `bus_trade_plate` VALUES ('346', '1', '2021-03-19 10:30:00');
INSERT INTO `bus_trade_plate` VALUES ('347', '1', '2021-03-19 10:30:00');
INSERT INTO `bus_trade_plate` VALUES ('348', '1', '2021-03-19 10:31:00');
INSERT INTO `bus_trade_plate` VALUES ('349', '1', '2021-03-19 10:31:00');
INSERT INTO `bus_trade_plate` VALUES ('350', '1', '2021-03-19 10:32:00');
INSERT INTO `bus_trade_plate` VALUES ('351', '1', '2021-03-19 10:32:00');
INSERT INTO `bus_trade_plate` VALUES ('352', '1', '2021-03-19 10:33:00');
INSERT INTO `bus_trade_plate` VALUES ('353', '1', '2021-03-19 10:33:00');
INSERT INTO `bus_trade_plate` VALUES ('354', '1', '2021-03-19 10:34:00');
INSERT INTO `bus_trade_plate` VALUES ('355', '1', '2021-03-19 10:34:00');
INSERT INTO `bus_trade_plate` VALUES ('356', '1', '2021-03-19 10:35:00');
INSERT INTO `bus_trade_plate` VALUES ('357', '1', '2021-03-19 10:35:00');
INSERT INTO `bus_trade_plate` VALUES ('358', '1', '2021-03-19 10:36:00');
INSERT INTO `bus_trade_plate` VALUES ('359', '1', '2021-03-19 10:36:00');
INSERT INTO `bus_trade_plate` VALUES ('360', '1', '2021-03-19 10:37:00');
INSERT INTO `bus_trade_plate` VALUES ('361', '1', '2021-03-19 10:37:00');
INSERT INTO `bus_trade_plate` VALUES ('362', '1', '2021-03-19 10:38:00');
INSERT INTO `bus_trade_plate` VALUES ('363', '1', '2021-03-19 10:38:00');
INSERT INTO `bus_trade_plate` VALUES ('364', '1', '2021-03-19 10:39:00');
INSERT INTO `bus_trade_plate` VALUES ('365', '1', '2021-03-19 10:39:00');
INSERT INTO `bus_trade_plate` VALUES ('366', '1', '2021-03-19 10:40:00');
INSERT INTO `bus_trade_plate` VALUES ('367', '1', '2021-03-19 10:40:00');
INSERT INTO `bus_trade_plate` VALUES ('368', '1', '2021-03-19 10:41:00');
INSERT INTO `bus_trade_plate` VALUES ('369', '1', '2021-03-19 10:41:00');
INSERT INTO `bus_trade_plate` VALUES ('370', '1', '2021-03-19 10:42:00');
INSERT INTO `bus_trade_plate` VALUES ('371', '1', '2021-03-19 10:42:00');
INSERT INTO `bus_trade_plate` VALUES ('372', '1', '2021-03-19 10:43:00');
INSERT INTO `bus_trade_plate` VALUES ('373', '1', '2021-03-19 10:43:00');
INSERT INTO `bus_trade_plate` VALUES ('374', '1', '2021-03-19 10:44:00');
INSERT INTO `bus_trade_plate` VALUES ('375', '1', '2021-03-19 10:44:00');
INSERT INTO `bus_trade_plate` VALUES ('376', '1', '2021-03-19 10:45:00');
INSERT INTO `bus_trade_plate` VALUES ('377', '1', '2021-03-19 10:45:00');
INSERT INTO `bus_trade_plate` VALUES ('378', '1', '2021-03-19 10:46:00');
INSERT INTO `bus_trade_plate` VALUES ('379', '1', '2021-03-19 10:46:00');
INSERT INTO `bus_trade_plate` VALUES ('380', '1', '2021-03-19 10:47:00');
INSERT INTO `bus_trade_plate` VALUES ('381', '1', '2021-03-19 10:47:00');
INSERT INTO `bus_trade_plate` VALUES ('382', '1', '2021-03-19 10:48:00');
INSERT INTO `bus_trade_plate` VALUES ('383', '1', '2021-03-19 10:48:00');
INSERT INTO `bus_trade_plate` VALUES ('384', '1', '2021-03-19 10:49:00');
INSERT INTO `bus_trade_plate` VALUES ('385', '1', '2021-03-19 10:49:00');
INSERT INTO `bus_trade_plate` VALUES ('386', '1', '2021-03-19 10:50:00');
INSERT INTO `bus_trade_plate` VALUES ('387', '1', '2021-03-19 10:50:00');
INSERT INTO `bus_trade_plate` VALUES ('388', '1', '2021-03-19 10:51:00');
INSERT INTO `bus_trade_plate` VALUES ('389', '1', '2021-03-19 10:51:00');
INSERT INTO `bus_trade_plate` VALUES ('390', '1', '2021-03-19 10:52:00');
INSERT INTO `bus_trade_plate` VALUES ('391', '1', '2021-03-19 10:52:00');
INSERT INTO `bus_trade_plate` VALUES ('392', '1', '2021-03-19 10:53:00');
INSERT INTO `bus_trade_plate` VALUES ('393', '1', '2021-03-19 10:53:00');
INSERT INTO `bus_trade_plate` VALUES ('394', '1', '2021-03-19 10:54:00');
INSERT INTO `bus_trade_plate` VALUES ('395', '1', '2021-03-19 10:54:00');
INSERT INTO `bus_trade_plate` VALUES ('396', '1', '2021-03-19 10:55:00');
INSERT INTO `bus_trade_plate` VALUES ('397', '1', '2021-03-19 10:55:00');
INSERT INTO `bus_trade_plate` VALUES ('398', '1', '2021-03-19 10:56:00');
INSERT INTO `bus_trade_plate` VALUES ('399', '1', '2021-03-19 10:56:00');
INSERT INTO `bus_trade_plate` VALUES ('400', '1', '2021-03-19 10:57:00');
INSERT INTO `bus_trade_plate` VALUES ('401', '1', '2021-03-19 10:57:00');
INSERT INTO `bus_trade_plate` VALUES ('402', '1', '2021-03-19 10:58:00');
INSERT INTO `bus_trade_plate` VALUES ('403', '1', '2021-03-19 10:58:00');
INSERT INTO `bus_trade_plate` VALUES ('404', '1', '2021-03-19 10:59:00');
INSERT INTO `bus_trade_plate` VALUES ('405', '1', '2021-03-19 10:59:00');
INSERT INTO `bus_trade_plate` VALUES ('406', '1', '2021-03-19 11:00:00');
INSERT INTO `bus_trade_plate` VALUES ('407', '1', '2021-03-19 11:00:00');
INSERT INTO `bus_trade_plate` VALUES ('408', '1', '2021-03-19 11:01:00');
INSERT INTO `bus_trade_plate` VALUES ('409', '1', '2021-03-19 11:01:00');
INSERT INTO `bus_trade_plate` VALUES ('410', '1', '2021-03-19 11:02:00');
INSERT INTO `bus_trade_plate` VALUES ('411', '1', '2021-03-19 11:02:00');
INSERT INTO `bus_trade_plate` VALUES ('412', '1', '2021-03-19 11:03:00');
INSERT INTO `bus_trade_plate` VALUES ('413', '1', '2021-03-19 11:03:00');
INSERT INTO `bus_trade_plate` VALUES ('414', '1', '2021-03-19 11:04:00');
INSERT INTO `bus_trade_plate` VALUES ('415', '1', '2021-03-19 11:04:00');
INSERT INTO `bus_trade_plate` VALUES ('416', '1', '2021-03-19 11:05:00');
INSERT INTO `bus_trade_plate` VALUES ('417', '1', '2021-03-19 11:05:00');
INSERT INTO `bus_trade_plate` VALUES ('418', '1', '2021-03-19 11:06:00');
INSERT INTO `bus_trade_plate` VALUES ('419', '1', '2021-03-19 11:06:00');
INSERT INTO `bus_trade_plate` VALUES ('420', '1', '2021-03-19 11:07:00');
INSERT INTO `bus_trade_plate` VALUES ('421', '1', '2021-03-19 11:07:00');
INSERT INTO `bus_trade_plate` VALUES ('422', '1', '2021-03-19 11:08:00');
INSERT INTO `bus_trade_plate` VALUES ('423', '1', '2021-03-19 11:10:00');
INSERT INTO `bus_trade_plate` VALUES ('424', '1', '2021-03-19 11:11:00');
INSERT INTO `bus_trade_plate` VALUES ('425', '1', '2021-03-19 11:12:00');
INSERT INTO `bus_trade_plate` VALUES ('426', '1', '2021-03-19 11:13:00');
INSERT INTO `bus_trade_plate` VALUES ('427', '1', '2021-03-19 11:14:00');
INSERT INTO `bus_trade_plate` VALUES ('428', '1', '2021-03-19 11:15:00');
INSERT INTO `bus_trade_plate` VALUES ('429', '1', '2021-03-19 11:16:00');
INSERT INTO `bus_trade_plate` VALUES ('430', '1', '2021-03-19 11:17:00');
INSERT INTO `bus_trade_plate` VALUES ('431', '1', '2021-03-19 11:18:00');
INSERT INTO `bus_trade_plate` VALUES ('432', '1', '2021-03-19 11:18:00');
INSERT INTO `bus_trade_plate` VALUES ('433', '1', '2021-03-19 11:19:00');
INSERT INTO `bus_trade_plate` VALUES ('434', '1', '2021-03-19 11:19:00');
INSERT INTO `bus_trade_plate` VALUES ('435', '1', '2021-03-19 11:20:00');
INSERT INTO `bus_trade_plate` VALUES ('436', '1', '2021-03-19 11:20:00');
INSERT INTO `bus_trade_plate` VALUES ('437', '1', '2021-03-19 11:21:00');
INSERT INTO `bus_trade_plate` VALUES ('438', '1', '2021-03-19 11:21:00');
INSERT INTO `bus_trade_plate` VALUES ('439', '1', '2021-03-19 11:22:00');
INSERT INTO `bus_trade_plate` VALUES ('440', '1', '2021-03-19 11:22:00');
INSERT INTO `bus_trade_plate` VALUES ('441', '1', '2021-03-19 11:23:00');
INSERT INTO `bus_trade_plate` VALUES ('442', '1', '2021-03-19 11:23:00');
INSERT INTO `bus_trade_plate` VALUES ('443', '1', '2021-03-19 11:24:00');
INSERT INTO `bus_trade_plate` VALUES ('444', '1', '2021-03-19 11:24:00');
INSERT INTO `bus_trade_plate` VALUES ('445', '1', '2021-03-19 11:25:00');
INSERT INTO `bus_trade_plate` VALUES ('446', '1', '2021-03-19 11:25:00');
INSERT INTO `bus_trade_plate` VALUES ('447', '1', '2021-03-19 11:26:00');
INSERT INTO `bus_trade_plate` VALUES ('448', '1', '2021-03-19 11:26:00');
INSERT INTO `bus_trade_plate` VALUES ('449', '1', '2021-03-19 11:27:00');
INSERT INTO `bus_trade_plate` VALUES ('450', '1', '2021-03-19 11:27:00');
INSERT INTO `bus_trade_plate` VALUES ('451', '1', '2021-03-19 11:28:00');
INSERT INTO `bus_trade_plate` VALUES ('452', '1', '2021-03-19 11:28:00');
INSERT INTO `bus_trade_plate` VALUES ('453', '1', '2021-03-19 11:29:00');
INSERT INTO `bus_trade_plate` VALUES ('454', '1', '2021-03-19 11:29:00');
INSERT INTO `bus_trade_plate` VALUES ('455', '1', '2021-03-19 11:30:00');
INSERT INTO `bus_trade_plate` VALUES ('456', '1', '2021-03-19 11:30:00');
INSERT INTO `bus_trade_plate` VALUES ('457', '1', '2021-03-19 11:31:00');
INSERT INTO `bus_trade_plate` VALUES ('458', '1', '2021-03-19 11:31:00');
INSERT INTO `bus_trade_plate` VALUES ('459', '1', '2021-03-19 11:32:00');
INSERT INTO `bus_trade_plate` VALUES ('460', '1', '2021-03-19 11:32:00');
INSERT INTO `bus_trade_plate` VALUES ('461', '1', '2021-03-19 11:33:00');
INSERT INTO `bus_trade_plate` VALUES ('462', '1', '2021-03-19 11:33:00');
INSERT INTO `bus_trade_plate` VALUES ('463', '1', '2021-03-19 11:34:00');
INSERT INTO `bus_trade_plate` VALUES ('464', '1', '2021-03-19 11:34:00');
INSERT INTO `bus_trade_plate` VALUES ('465', '1', '2021-03-19 11:35:00');
INSERT INTO `bus_trade_plate` VALUES ('466', '1', '2021-03-19 11:35:00');
INSERT INTO `bus_trade_plate` VALUES ('467', '1', '2021-03-19 11:36:00');
INSERT INTO `bus_trade_plate` VALUES ('468', '1', '2021-03-19 11:36:00');
INSERT INTO `bus_trade_plate` VALUES ('469', '1', '2021-03-19 11:37:00');
INSERT INTO `bus_trade_plate` VALUES ('470', '1', '2021-03-19 11:37:00');
INSERT INTO `bus_trade_plate` VALUES ('471', '1', '2021-03-19 11:38:00');
INSERT INTO `bus_trade_plate` VALUES ('472', '1', '2021-03-19 11:38:00');
INSERT INTO `bus_trade_plate` VALUES ('473', '1', '2021-03-19 11:39:00');
INSERT INTO `bus_trade_plate` VALUES ('474', '1', '2021-03-19 11:39:00');
INSERT INTO `bus_trade_plate` VALUES ('475', '1', '2021-03-19 11:40:00');
INSERT INTO `bus_trade_plate` VALUES ('476', '1', '2021-03-19 11:40:00');
INSERT INTO `bus_trade_plate` VALUES ('477', '1', '2021-03-19 11:41:00');
INSERT INTO `bus_trade_plate` VALUES ('478', '1', '2021-03-19 11:41:00');
INSERT INTO `bus_trade_plate` VALUES ('479', '1', '2021-03-19 11:42:00');
INSERT INTO `bus_trade_plate` VALUES ('480', '1', '2021-03-19 11:42:00');
INSERT INTO `bus_trade_plate` VALUES ('481', '1', '2021-03-19 11:43:00');
INSERT INTO `bus_trade_plate` VALUES ('482', '1', '2021-03-19 11:43:00');
INSERT INTO `bus_trade_plate` VALUES ('483', '1', '2021-03-19 11:44:00');
INSERT INTO `bus_trade_plate` VALUES ('484', '1', '2021-03-19 11:44:00');
INSERT INTO `bus_trade_plate` VALUES ('485', '1', '2021-03-19 11:45:00');
INSERT INTO `bus_trade_plate` VALUES ('486', '1', '2021-03-19 11:45:00');
INSERT INTO `bus_trade_plate` VALUES ('487', '1', '2021-03-19 11:46:00');
INSERT INTO `bus_trade_plate` VALUES ('488', '1', '2021-03-19 11:46:00');
INSERT INTO `bus_trade_plate` VALUES ('489', '1', '2021-03-19 11:47:00');
INSERT INTO `bus_trade_plate` VALUES ('490', '1', '2021-03-19 11:47:00');
INSERT INTO `bus_trade_plate` VALUES ('491', '1', '2021-03-19 11:48:00');
INSERT INTO `bus_trade_plate` VALUES ('492', '1', '2021-03-19 11:48:00');
INSERT INTO `bus_trade_plate` VALUES ('493', '1', '2021-03-19 11:49:00');
INSERT INTO `bus_trade_plate` VALUES ('494', '1', '2021-03-19 11:49:00');
INSERT INTO `bus_trade_plate` VALUES ('495', '1', '2021-03-19 11:50:00');
INSERT INTO `bus_trade_plate` VALUES ('496', '1', '2021-03-19 11:50:00');
INSERT INTO `bus_trade_plate` VALUES ('497', '1', '2021-03-19 11:51:00');
INSERT INTO `bus_trade_plate` VALUES ('498', '1', '2021-03-19 11:51:00');
INSERT INTO `bus_trade_plate` VALUES ('499', '1', '2021-03-19 11:52:00');
INSERT INTO `bus_trade_plate` VALUES ('500', '1', '2021-03-19 11:52:00');
INSERT INTO `bus_trade_plate` VALUES ('501', '1', '2021-03-19 11:53:00');
INSERT INTO `bus_trade_plate` VALUES ('502', '1', '2021-03-19 11:53:00');
INSERT INTO `bus_trade_plate` VALUES ('503', '1', '2021-03-19 11:54:00');
INSERT INTO `bus_trade_plate` VALUES ('504', '1', '2021-03-19 11:54:00');
INSERT INTO `bus_trade_plate` VALUES ('505', '1', '2021-03-19 11:55:00');
INSERT INTO `bus_trade_plate` VALUES ('506', '1', '2021-03-19 11:55:00');
INSERT INTO `bus_trade_plate` VALUES ('507', '1', '2021-03-19 11:56:00');
INSERT INTO `bus_trade_plate` VALUES ('508', '1', '2021-03-19 11:56:00');
INSERT INTO `bus_trade_plate` VALUES ('509', '1', '2021-03-19 11:57:00');
INSERT INTO `bus_trade_plate` VALUES ('510', '1', '2021-03-19 11:57:00');
INSERT INTO `bus_trade_plate` VALUES ('511', '1', '2021-03-19 11:58:00');
INSERT INTO `bus_trade_plate` VALUES ('512', '1', '2021-03-19 11:58:00');
INSERT INTO `bus_trade_plate` VALUES ('513', '1', '2021-03-19 11:59:00');
INSERT INTO `bus_trade_plate` VALUES ('514', '1', '2021-03-19 11:59:00');
INSERT INTO `bus_trade_plate` VALUES ('515', '1', '2021-03-19 12:00:00');
INSERT INTO `bus_trade_plate` VALUES ('516', '1', '2021-03-19 12:01:00');
INSERT INTO `bus_trade_plate` VALUES ('517', '1', '2021-03-19 12:02:00');
INSERT INTO `bus_trade_plate` VALUES ('518', '1', '2021-03-19 12:03:00');
INSERT INTO `bus_trade_plate` VALUES ('519', '1', '2021-03-19 12:04:00');
INSERT INTO `bus_trade_plate` VALUES ('520', '1', '2021-03-19 12:05:00');
INSERT INTO `bus_trade_plate` VALUES ('521', '1', '2021-03-19 12:06:00');
INSERT INTO `bus_trade_plate` VALUES ('522', '1', '2021-03-19 12:07:00');
INSERT INTO `bus_trade_plate` VALUES ('523', '1', '2021-03-19 12:07:00');
INSERT INTO `bus_trade_plate` VALUES ('524', '1', '2021-03-19 12:08:00');
INSERT INTO `bus_trade_plate` VALUES ('525', '1', '2021-03-19 12:08:00');
INSERT INTO `bus_trade_plate` VALUES ('526', '1', '2021-03-19 12:09:00');
INSERT INTO `bus_trade_plate` VALUES ('527', '1', '2021-03-19 12:09:00');
INSERT INTO `bus_trade_plate` VALUES ('528', '1', '2021-03-19 12:10:00');
INSERT INTO `bus_trade_plate` VALUES ('529', '1', '2021-03-19 12:10:00');
INSERT INTO `bus_trade_plate` VALUES ('530', '1', '2021-03-19 12:11:00');
INSERT INTO `bus_trade_plate` VALUES ('531', '1', '2021-03-19 12:12:00');
INSERT INTO `bus_trade_plate` VALUES ('532', '1', '2021-03-19 12:13:00');
INSERT INTO `bus_trade_plate` VALUES ('533', '1', '2021-03-19 12:13:00');
INSERT INTO `bus_trade_plate` VALUES ('534', '1', '2021-03-19 12:14:00');
INSERT INTO `bus_trade_plate` VALUES ('535', '1', '2021-03-19 12:15:00');
INSERT INTO `bus_trade_plate` VALUES ('536', '1', '2021-03-19 12:16:00');
INSERT INTO `bus_trade_plate` VALUES ('537', '1', '2021-03-19 12:17:00');
INSERT INTO `bus_trade_plate` VALUES ('538', '1', '2021-03-19 12:18:00');
INSERT INTO `bus_trade_plate` VALUES ('539', '1', '2021-03-19 12:19:00');
INSERT INTO `bus_trade_plate` VALUES ('540', '1', '2021-03-19 12:20:00');
INSERT INTO `bus_trade_plate` VALUES ('541', '1', '2021-03-19 12:21:00');
INSERT INTO `bus_trade_plate` VALUES ('542', '1', '2021-03-19 12:22:00');
INSERT INTO `bus_trade_plate` VALUES ('543', '1', '2021-03-19 12:23:00');
INSERT INTO `bus_trade_plate` VALUES ('544', '1', '2021-03-19 12:24:00');
INSERT INTO `bus_trade_plate` VALUES ('545', '1', '2021-03-19 12:25:00');
INSERT INTO `bus_trade_plate` VALUES ('546', '1', '2021-03-19 12:26:00');
INSERT INTO `bus_trade_plate` VALUES ('547', '1', '2021-03-19 12:27:00');
INSERT INTO `bus_trade_plate` VALUES ('548', '1', '2021-03-19 12:28:00');
INSERT INTO `bus_trade_plate` VALUES ('549', '1', '2021-03-19 12:29:00');
INSERT INTO `bus_trade_plate` VALUES ('550', '1', '2021-03-19 12:30:00');
INSERT INTO `bus_trade_plate` VALUES ('551', '1', '2021-03-19 12:31:00');
INSERT INTO `bus_trade_plate` VALUES ('552', '1', '2021-03-19 12:32:00');
INSERT INTO `bus_trade_plate` VALUES ('553', '1', '2021-03-19 12:33:00');
INSERT INTO `bus_trade_plate` VALUES ('554', '1', '2021-03-19 12:34:00');
INSERT INTO `bus_trade_plate` VALUES ('555', '1', '2021-03-19 12:35:00');
INSERT INTO `bus_trade_plate` VALUES ('556', '1', '2021-03-19 12:36:00');
INSERT INTO `bus_trade_plate` VALUES ('557', '1', '2021-03-19 12:37:00');
INSERT INTO `bus_trade_plate` VALUES ('558', '1', '2021-03-19 12:38:00');
INSERT INTO `bus_trade_plate` VALUES ('559', '1', '2021-03-19 12:39:00');
INSERT INTO `bus_trade_plate` VALUES ('560', '1', '2021-03-19 12:40:00');
INSERT INTO `bus_trade_plate` VALUES ('561', '1', '2021-03-19 12:41:00');
INSERT INTO `bus_trade_plate` VALUES ('562', '1', '2021-03-19 12:42:00');
INSERT INTO `bus_trade_plate` VALUES ('563', '1', '2021-03-19 12:43:00');
INSERT INTO `bus_trade_plate` VALUES ('564', '1', '2021-03-19 12:44:00');
INSERT INTO `bus_trade_plate` VALUES ('565', '1', '2021-03-19 12:45:00');
INSERT INTO `bus_trade_plate` VALUES ('566', '1', '2021-03-19 12:46:00');
INSERT INTO `bus_trade_plate` VALUES ('567', '1', '2021-03-19 12:47:00');
INSERT INTO `bus_trade_plate` VALUES ('568', '1', '2021-03-19 12:48:00');
INSERT INTO `bus_trade_plate` VALUES ('569', '1', '2021-03-19 12:49:00');
INSERT INTO `bus_trade_plate` VALUES ('570', '1', '2021-03-19 12:50:00');
INSERT INTO `bus_trade_plate` VALUES ('571', '1', '2021-03-19 12:51:00');
INSERT INTO `bus_trade_plate` VALUES ('572', '1', '2021-03-19 12:52:00');
INSERT INTO `bus_trade_plate` VALUES ('573', '1', '2021-03-19 12:53:00');
INSERT INTO `bus_trade_plate` VALUES ('574', '1', '2021-03-19 12:54:00');
INSERT INTO `bus_trade_plate` VALUES ('575', '1', '2021-03-19 12:55:00');
INSERT INTO `bus_trade_plate` VALUES ('576', '1', '2021-03-19 12:56:00');
INSERT INTO `bus_trade_plate` VALUES ('577', '1', '2021-03-19 12:57:00');
INSERT INTO `bus_trade_plate` VALUES ('578', '1', '2021-03-19 12:58:00');
INSERT INTO `bus_trade_plate` VALUES ('579', '1', '2021-03-19 12:59:00');
INSERT INTO `bus_trade_plate` VALUES ('580', '1', '2021-03-19 13:00:00');
INSERT INTO `bus_trade_plate` VALUES ('581', '1', '2021-03-19 13:01:00');
INSERT INTO `bus_trade_plate` VALUES ('582', '1', '2021-03-19 13:02:00');
INSERT INTO `bus_trade_plate` VALUES ('583', '1', '2021-03-19 13:03:00');
INSERT INTO `bus_trade_plate` VALUES ('584', '1', '2021-03-19 13:04:00');
INSERT INTO `bus_trade_plate` VALUES ('585', '1', '2021-03-19 13:05:00');
INSERT INTO `bus_trade_plate` VALUES ('586', '1', '2021-03-19 13:06:00');
INSERT INTO `bus_trade_plate` VALUES ('587', '1', '2021-03-19 13:07:00');
INSERT INTO `bus_trade_plate` VALUES ('588', '1', '2021-03-19 13:08:00');
INSERT INTO `bus_trade_plate` VALUES ('589', '1', '2021-03-19 13:09:00');
INSERT INTO `bus_trade_plate` VALUES ('590', '1', '2021-03-19 13:10:00');
INSERT INTO `bus_trade_plate` VALUES ('591', '1', '2021-03-19 13:11:00');
INSERT INTO `bus_trade_plate` VALUES ('592', '1', '2021-03-19 13:12:00');
INSERT INTO `bus_trade_plate` VALUES ('593', '1', '2021-03-19 13:13:00');
INSERT INTO `bus_trade_plate` VALUES ('594', '1', '2021-03-19 13:14:00');
INSERT INTO `bus_trade_plate` VALUES ('595', '1', '2021-03-19 13:15:00');
INSERT INTO `bus_trade_plate` VALUES ('596', '1', '2021-03-19 13:16:00');
INSERT INTO `bus_trade_plate` VALUES ('597', '1', '2021-03-19 13:17:00');
INSERT INTO `bus_trade_plate` VALUES ('598', '1', '2021-03-19 13:18:00');
INSERT INTO `bus_trade_plate` VALUES ('599', '1', '2021-03-19 13:19:00');
INSERT INTO `bus_trade_plate` VALUES ('600', '1', '2021-03-19 13:20:00');
INSERT INTO `bus_trade_plate` VALUES ('601', '1', '2021-03-19 13:21:00');
INSERT INTO `bus_trade_plate` VALUES ('602', '1', '2021-03-19 13:22:00');
INSERT INTO `bus_trade_plate` VALUES ('603', '1', '2021-03-19 13:23:00');
INSERT INTO `bus_trade_plate` VALUES ('604', '1', '2021-03-19 13:24:00');
INSERT INTO `bus_trade_plate` VALUES ('605', '1', '2021-03-19 13:25:00');
INSERT INTO `bus_trade_plate` VALUES ('606', '1', '2021-03-19 13:26:00');
INSERT INTO `bus_trade_plate` VALUES ('607', '1', '2021-03-19 13:27:00');
INSERT INTO `bus_trade_plate` VALUES ('608', '1', '2021-03-19 13:28:00');
INSERT INTO `bus_trade_plate` VALUES ('609', '1', '2021-03-19 13:29:00');
INSERT INTO `bus_trade_plate` VALUES ('610', '1', '2021-03-19 13:30:00');
INSERT INTO `bus_trade_plate` VALUES ('611', '1', '2021-03-19 13:31:00');
INSERT INTO `bus_trade_plate` VALUES ('612', '1', '2021-03-19 13:32:00');
INSERT INTO `bus_trade_plate` VALUES ('613', '1', '2021-03-19 13:33:00');
INSERT INTO `bus_trade_plate` VALUES ('614', '1', '2021-03-19 13:34:00');
INSERT INTO `bus_trade_plate` VALUES ('615', '1', '2021-03-19 13:35:00');
INSERT INTO `bus_trade_plate` VALUES ('616', '1', '2021-03-19 13:36:00');
INSERT INTO `bus_trade_plate` VALUES ('617', '1', '2021-03-19 13:37:00');
INSERT INTO `bus_trade_plate` VALUES ('618', '1', '2021-03-19 13:38:00');
INSERT INTO `bus_trade_plate` VALUES ('619', '1', '2021-03-19 13:39:00');
INSERT INTO `bus_trade_plate` VALUES ('620', '1', '2021-03-19 13:40:00');
INSERT INTO `bus_trade_plate` VALUES ('621', '1', '2021-03-19 13:40:00');
INSERT INTO `bus_trade_plate` VALUES ('622', '1', '2021-03-19 13:40:00');
INSERT INTO `bus_trade_plate` VALUES ('623', '1', '2021-03-19 13:41:00');
INSERT INTO `bus_trade_plate` VALUES ('624', '1', '2021-03-19 13:41:00');
INSERT INTO `bus_trade_plate` VALUES ('625', '1', '2021-03-19 13:42:00');
INSERT INTO `bus_trade_plate` VALUES ('626', '1', '2021-03-19 13:42:00');
INSERT INTO `bus_trade_plate` VALUES ('627', '1', '2021-03-19 13:43:00');
INSERT INTO `bus_trade_plate` VALUES ('628', '1', '2021-03-19 13:43:00');
INSERT INTO `bus_trade_plate` VALUES ('629', '1', '2021-03-19 13:44:00');
INSERT INTO `bus_trade_plate` VALUES ('630', '1', '2021-03-19 13:44:00');
INSERT INTO `bus_trade_plate` VALUES ('631', '1', '2021-03-19 13:45:00');
INSERT INTO `bus_trade_plate` VALUES ('632', '1', '2021-03-19 13:45:00');
INSERT INTO `bus_trade_plate` VALUES ('633', '1', '2021-03-19 13:46:00');
INSERT INTO `bus_trade_plate` VALUES ('634', '1', '2021-03-19 13:46:00');

-- ----------------------------
-- Table structure for bus_user_good
-- ----------------------------
DROP TABLE IF EXISTS `bus_user_good`;
CREATE TABLE `bus_user_good` (
  `user_id` int(11) DEFAULT NULL,
  `good_id` int(11) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL,
  UNIQUE KEY `user_id, good_id` (`user_id`,`good_id`) USING BTREE COMMENT '用户 物品唯一性'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci COMMENT='用户物品持有表';

-- ----------------------------
-- Records of bus_user_good
-- ----------------------------
INSERT INTO `bus_user_good` VALUES ('47', '1', '200');
INSERT INTO `bus_user_good` VALUES ('48', '1', '0');
INSERT INTO `bus_user_good` VALUES ('49', '1', '0');
INSERT INTO `bus_user_good` VALUES ('1', '1', '300');
INSERT INTO `bus_user_good` VALUES ('50', '1', '0');

-- ----------------------------
-- Table structure for sys_good
-- ----------------------------
DROP TABLE IF EXISTS `sys_good`;
CREATE TABLE `sys_good` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL COMMENT '名称',
  `desc` varchar(255) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1' COMMENT '0 nouse  1use',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci COMMENT='拍卖的物品表';

-- ----------------------------
-- Records of sys_good
-- ----------------------------
INSERT INTO `sys_good` VALUES ('1', '平安证券', '', '1');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `pass_word` varchar(255) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `fund` double(255,0) DEFAULT '0' COMMENT '资产',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci COMMENT='系统用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '1', '1200', '2021-03-18 16:14:08', '2021-03-18 16:57:00');
INSERT INTO `sys_user` VALUES ('47', '1', '1', '800', '2021-03-18 16:17:15', '2021-03-18 17:06:25');
INSERT INTO `sys_user` VALUES ('48', '2', '1', '1000', '2021-03-18 16:17:22', '2021-03-18 17:06:25');
INSERT INTO `sys_user` VALUES ('49', '3', '1', '0', '2021-03-18 16:53:16', null);
INSERT INTO `sys_user` VALUES ('50', '6', '1', '1', '2021-03-18 16:56:27', '2021-03-19 12:13:33');
